//
//  Variable.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

/// Variable class
/// Can hold a value and notify observers upon update

public class Variable<T> {
    
    public typealias ObserverClosure = (T) -> Void
    public private(set) var value: T?
    private var observers = [ObserverClosure]()
    
    /// Can be initialized with empty initial value
    public init(_ value: T? = nil) {
        self.value = value
    }
    
    /// Update Variable with new value and notify observers
    /// If new value equals old one do nothing
    public func setValue(_ value: T) {
        self.value = value
        observers.forEach { $0(value) }
    }
    
    /// Add an observer
    public func observe(_ observer: @escaping ObserverClosure) {
        observers.append(observer)
        if let value = self.value {
            observer(value)
        }
    }
}
