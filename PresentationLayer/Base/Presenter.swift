//
//  Presenter.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift


/// Base presenter class
/// To start presenting anything, attach view to Presenter
/// To stop, detach view
public class Presenter<V: ViewIO> {
    
    private(set) weak var view: V?
    
    private(set) var disposeBag: DisposeBag?
    fileprivate var activityIndicator: ActivityIndicator?
    private var isSetup = false

    
    /// Attaches view to presenter; presenter will subscribe to outputs of View and performs all its internal operations after attachView is called
    ///
    /// - Parameter view: view to attach
    final public func attachView(view: V){
        self.view = view
        //clears previous dispose bag, causing all subscriptions to be cleaned up
        self.disposeBag = DisposeBag()
        
        //allow tracking of Observable activity status
        activityIndicator = ActivityIndicator()
        activityIndicator?.asDriver().drive(onNext: { inProgress in
            self.onOperation(inProgress: inProgress)
        }).addDisposableTo(disposeBag!)

        viewAttached()

        if !isSetup {
            setup()
            isSetup = true
        }
    }
    
    
    /// Detach a view from a preseter; after that presenter will be mostly cleaned up
    final public func detachView(){
        self.view = nil
        self.activityIndicator = nil
        self.disposeBag = nil
    }
    
    
    /// Callback invoked by internal activity tracking facility in case some Observable is tranforme using Observable#trackActivity(_ presenter: Presenter)
    ///
    /// - Parameter inProgress: true if there is something loading out there
    public func onOperation(inProgress: Bool){
        view?.show(loading: inProgress)
    }
    
    internal func viewAttached() {
        fatalError("This function should be overriden")
    }
    
    internal func viewDetached() {
        //no op
    }
    
    /// Setups presenter's initial state, start async operation, etc
    /// Called only once upon `attachView`
    internal func setup() {
        //no op
    }

}

extension ObservableConvertibleType {
    
    /// Track activity of Observable with presenter
    ///
    /// - Parameter presenter: presenter to track the observable completion
    /// - Returns: transformed Observable that will notify presenter about completion status
    public func trackActivity<V>(_ presenter: Presenter<V>) -> Observable<E> {
        guard let indicator = presenter.activityIndicator else {
            fatalError("tracking activity of observable with inactive presenter")
        }
        return indicator.trackActivityOfObservable(self)
    
    }
}
