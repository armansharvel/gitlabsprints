//
//  View.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

/// Marker protocol for views
// sourcery: mock
public protocol ViewIO : class {
    /// Indicate loading progress
    ///
    /// - Parameter loading: whether there is blocking operation going on
    func show(loading: Bool)
}
