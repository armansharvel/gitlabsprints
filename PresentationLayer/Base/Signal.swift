//
//  Signal.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

/// Signal class
/// Can notify observers upon update

public class Signal<T> {
    
    public typealias ObserverClosure = (T) -> Void
    private var observers = [ObserverClosure]()
    
    public init() {
    }
    
    /// Update Variable with new value and notify observers
    public func send(_ value: T) {
        observers.forEach { $0(value) }
    }
    
    /// Add an observer
    public func observe(_ observer: @escaping ObserverClosure) {
        observers.append(observer)
    }
}
