//
//  RecoverableError.swift
//  glsprints
//
//  Created by Andrew Ousenko on 19/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation


/// An error with description that could optionally be recovered from
public struct ErrorWithRecovery: Error {
    
    private(set) var recoveryOption: RecoveryOption?
    
    public let errorDescription: String
    
    
    ///
    /// - Parameters:
    ///   - errorDescription: meaningful (to user) description
    ///   - recoveryOption: optional recovery (e.g. user taps on button "Retry")
    public init(_ errorDescription: String, _ recoveryOption: RecoveryOption? = nil){
        self.errorDescription = errorDescription
        self.recoveryOption = recoveryOption
    }
}


/// Option to recover from error, with descriptive name
public struct RecoveryOption {
    public let recoveryAction: (()->Void)
    public let recoveryOptionName: String
    
    public init(recoveryAction: @escaping (()->Void), recoveryOptionName: String){
        self.recoveryAction = recoveryAction
        self.recoveryOptionName = recoveryOptionName
    }
}
