//
//  SprintsViewIO.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import RxSwift
import RxCocoa
import DomainLayer

public protocol SprintsViewIO: ViewIO {

    /// User taps back button
	var backButtonPressed: Signal<Void> { get }

    /// User selects sprint cell
    /// - Return: Sprint id
    var tableViewDidSelectSprint: Driver<Int> { get }

    /// Show error
    func showLoadingError(_ error: ErrorWithRecovery)

    /// Show available sprint
    func showSprints(_ sprints: [Sprint])

    /// Show current group
	func showGroupInfo(_ group: Group)
}
