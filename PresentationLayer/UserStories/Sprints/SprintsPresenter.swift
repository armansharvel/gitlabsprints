//
//  SprintsPresenter.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer
import RxSwift

public class SprintsPresenter<V: SprintsViewIO>: Presenter<V> {
    
    private struct State {
        let group = Variable<Group>()
        let sprints = Variable<[Sprint]>()
    }
    
    private let interactor: SprintsInteractor
    private let navigator: SprintsNavigator
    private let state = State()
    
    public init(interactor: SprintsInteractor, navigator: SprintsNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    override func setup() {
        interactor.getGroup()
            .subscribe(
                onNext: { [weak self] in self?.state.group.setValue($0) },
                onError: { [weak self] in self?.view?.showLoadingError(ErrorWithRecovery("Error: \($0)")) }
            )
            .disposed(by: disposeBag!)
        
        interactor.listSprints()
            .trackActivity(self)
            .subscribe(
                onNext: { [weak self] in self?.state.sprints.setValue($0) },
                onError: { [weak self] in self?.view?.showLoadingError(ErrorWithRecovery("Error: \($0)")) }
            )
            .disposed(by: disposeBag!)
    }
    
    override func viewAttached() {
        guard let viewIO = self.view else { return }
        
        state.group.observe { viewIO.showGroupInfo($0) }
        state.sprints.observe { viewIO.showSprints($0) }
        
        viewIO.backButtonPressed.observe { [weak self] _ in
			self?.navigator.navigateBackToGroups()
		}
        
        viewIO.tableViewDidSelectSprint
            .drive(
                onNext: { [weak self] sprintId in
                    guard let `self` = self else { return }
                    guard let sprint = self.state.sprints.value?.first(where: { $0.uid == sprintId }) else { return }
                    self.navigator.navigateToProjects(sprint: sprint, group: self.interactor.group)
            })
            .disposed(by: disposeBag!)
        
    }
}
