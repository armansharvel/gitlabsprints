//
//  SprintsInteractor.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class SprintsInteractor: Interactor {

    private let repo: SprintsRepository
    let group: Group
    
    public init(executors: Executors, repo: SprintsRepository, group: Group) {
        self.repo = repo
        self.group = group
        super.init(executors: executors)
    }

    public func getGroup() -> Observable<Group> {
        return Observable.just(group)
    }
    
    public func listSprints() -> Observable<[Sprint]> {
        return applySchedulers(
            repo.listSprints(ofGroup: group)
        )
    }
}
