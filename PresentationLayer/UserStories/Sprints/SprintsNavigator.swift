//
//  SprintsNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer

public protocol SprintsNavigator {
    func navigateToProjects(sprint: Sprint, group: Group)
	func navigateBackToGroups()
}
