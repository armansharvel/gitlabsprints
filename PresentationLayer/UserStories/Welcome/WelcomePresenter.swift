//
//  WelcomePresenter.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift


/// Presents a welcome screen where user is about to log in into GitLab to see available groups
public class WelcomePresenter<V: WelcomeViewIO>: Presenter<V> {

    private let interactor: WelcomeInteractor
    /*
     TODO: internals of this navigator should be weak, so that all internal UIViews, UIViewControllers could be gone when view is detached, and actions invoked on navigator would not have an effect
     */
    private let navigator: WelcomeNavigator
    
    public init(interactor: WelcomeInteractor, navigator: WelcomeNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    
    override func viewAttached() {
        Observable.zip(interactor.isSignedIn(), interactor.hasFollowingGroups()) { ($0, $1) }
            .subscribe(
                onNext: { [weak self] (isSignedIn, hasFollowingGroups) in
                    if !isSignedIn {
                        return
                    }
                    else if !hasFollowingGroups {
                        self?.navigator.navigateToGroupsSelection()
                    }
                    else {
                        self?.navigator.navigateToDashboard()
                    }
                }
            )
            .disposed(by: disposeBag!)

        view?.logInClicked.observe { [weak self] _ in
            self?.navigator.navigateToAuth()
        }
    }
    
}
