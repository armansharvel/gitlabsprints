//
//  WelcomeView.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation


/// View that welcomes user :-)
/// Contains a greeting and a button, tapping on which user attempts to sign into GitLab account
// sourcery: mock
public protocol WelcomeViewIO: ViewIO {
    
    /// User clicks login button
    var logInClicked: Signal<Void> { get }
}
