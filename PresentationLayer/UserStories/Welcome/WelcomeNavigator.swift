//
//  WelcomeNavigator.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation


/// Welcome screen navigator
// sourcery: mock
public protocol WelcomeNavigator {
    func navigateToAuth()
    func navigateToGroupsSelection()
    func navigateToDashboard()
}
