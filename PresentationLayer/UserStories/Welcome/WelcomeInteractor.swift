//
//  WelcomeInteractor.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer


/// Welcome screen interactor
public class WelcomeInteractor : Interactor {

    private let authRepo: AuthRepository
    private let groupsRepo: GroupsRepository

    public init(executors: Executors, authRepo: AuthRepository, groupsRepo: GroupsRepository) {
        self.authRepo = authRepo
        self.groupsRepo = groupsRepo
        super.init(executors: executors)
    }
    
    public func isSignedIn() -> Observable<Bool> {
        return applySchedulers(
            authRepo.isSignedIn()
        )
    }

    public func hasFollowingGroups() -> Observable<Bool> {
        return applySchedulers(
            groupsRepo.loadFollowingGroups()
                .map { $0.count > 0 }
        )
    }
}
