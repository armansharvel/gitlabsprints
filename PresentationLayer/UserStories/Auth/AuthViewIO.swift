//
//  AuthView.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation

/// View that logins a user
/// Contains two text fields for username and password, and a button, tapping on which user attempts to sign into GitLab account
// sourcery: mock
public protocol AuthViewIO: ViewIO {

    /// Username input
    var username: Signal<String> { get }

    /// Password input
    var password: Signal<String> { get }

    /// User clicks login button
    var logInClicked: Signal<Void> { get }
    
    /// Login error occured
    func showLoginError(_ error: ErrorWithRecovery)
}
