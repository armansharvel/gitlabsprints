//
//  WelcomePresenter.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift


/// Presents a auth/login screen where user is about to log in into GitLab to see available groups
public class AuthPresenter<V: AuthViewIO>: Presenter<V> {

    struct State {
        let username = Variable<String>()
        let password = Variable<String>()
    }

    private let interactor: AuthInteractor
    private let navigator: AuthNavigator
    private let state: State
    
    public init(interactor: AuthInteractor, navigator: AuthNavigator) {
        self.interactor = interactor
        self.navigator = navigator
        self.state = State()
    }
    
    override func viewAttached() {
        guard let view = self.view else { return }

        view.username.observe { [weak self] in self?.state.username.setValue($0) }
        view.password.observe { [weak self] in self?.state.password.setValue($0) }

        view.logInClicked.observe { [weak self] _ in
            guard let `self` = self else { return }
            guard let username = self.state.username.value,
                  let password = self.state.password.value
            else { return }
            
            self.interactor.logIn(email: username, password: password)
                .trackActivity(self)
                .subscribe(
                    onNext: { [weak self] token in
                        print("auth token: \(token)")
                        self?.navigator.navigateToGroupSelection()

                },
                    onError: { [weak self] error in
                        self?.view?.showLoginError(ErrorWithRecovery("Some error occurred"))
                })
                .disposed(by: self.disposeBag!)
        }
    }
    
}
