//
//  AuthInteractor.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer


/// Auth screen interactor
public class AuthInteractor : Interactor {
    
    private let repo: AuthRepository
    
    public init(executors: Executors, repo: AuthRepository) {
        self.repo = repo
        super.init(executors: executors)
    }
    
    
    /// Perform log in into GitLab
    ///
    /// - Returns: Access token
    func logIn(email: String, password: String) -> Observable<String> {
        return applySchedulers(
            repo.signIn(email: email, password: password)
        )
    }

}
