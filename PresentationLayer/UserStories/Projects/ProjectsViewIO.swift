//
//  ProjectsViewIO.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import DomainLayer

public protocol ProjectsViewIO: ViewIO {

    /// User taps back button
    var backButtonPressed: Signal<Void> { get }

    /// User taps tracker button
    var trackerButtonPressed: Signal<Void> { get }

    /// Show error
    func showLoadingError(_ error: ErrorWithRecovery)

    /// Show sprint info
    func showSprintInfo(_ sprint: Sprint)

    /// Show projects info
    func showProjects(_ projects: [Project])

    /// Show sprint goals
    func showGoals(_ goals: [Issue])

}
