//
//  ProjectsNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer

public protocol ProjectsNavigator {
    func navigateBackToSprints()
    func navigateToTracker(sprint: Sprint, group: Group)
}
