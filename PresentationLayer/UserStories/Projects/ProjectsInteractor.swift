//
//  ProjectsInteractor.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class ProjectsInteractor: Interactor {
    
    private let repo: ProjectsRepository
    let group: Group
    let sprint: Sprint
    
    public init(executors: Executors, repo: ProjectsRepository, sprint: Sprint, group: Group) {
        self.repo = repo
        self.sprint = sprint
        self.group = group
        super.init(executors: executors)
    }

    public func getSprint() -> Observable<Sprint> {
        return Observable.just(sprint)
    }
    
    public func listProjects() -> Observable<[Project]> {
        return applySchedulers(
            repo.listProjects(of: sprint)
        )
    }
    
    public func listGoals() -> Observable<[Issue]> {
        return applySchedulers(
            repo.listGoals(of: sprint)
        )
    }
}
