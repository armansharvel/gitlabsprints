//
//  ProjectsPresenter.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class ProjectsPresenter<V: ProjectsViewIO>: Presenter<V> {
    
    private struct State {
        let sprint = Variable<Sprint>()
        let projects = Variable<[Project]>()
        let goals = Variable<[Issue]>()
    }
    
    private let interactor: ProjectsInteractor
    private let navigator: ProjectsNavigator
    private let state = State()
    
    public init(interactor: ProjectsInteractor, navigator: ProjectsNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    override func setup() {
        interactor.listGoals()
            .subscribe(
                onNext: { [weak self] in self?.state.goals.setValue($0) }
            )
            .disposed(by: disposeBag!)
        
        interactor.getSprint()
            .subscribe(
                onNext: { [weak self] in self?.state.sprint.setValue($0) }
            )
            .disposed(by: disposeBag!)
        
        interactor.listProjects()
            .trackActivity(self)
            .subscribe(
                onNext: { [weak self] in self?.state.projects.setValue($0) },
                onError: { [weak self] in self?.view?.showLoadingError(ErrorWithRecovery("Error: \($0)")) })
            .disposed(by: disposeBag!)
    }
    
    override func viewAttached() {
        guard let viewIO = self.view else { return }
        
        state.goals.observe { viewIO.showGoals($0) }
        state.sprint.observe { viewIO.showSprintInfo($0) }
        state.projects.observe { viewIO.showProjects($0) }
        
        viewIO.backButtonPressed.observe { [weak self] _ in
            self?.navigator.navigateBackToSprints()
        }
        
        viewIO.trackerButtonPressed.observe { [weak self] _ in
            guard let `self` = self else { return }
            self.navigator.navigateToTracker(sprint: self.interactor.sprint, group: self.interactor.group)
        }
    }
}
