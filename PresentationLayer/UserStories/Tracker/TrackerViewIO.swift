//
//  TrackerViewIO.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import DomainLayer

public protocol TrackerViewIO: ViewIO {

    /// User taps back button
    var backButtonPressed: Signal<Void> { get }

    /// Show error
    func showLoadingError(_ error: ErrorWithRecovery)

    /// Show sprint info
    func setSprintInfo(_ sprint: Sprint)

    /// Show sprint members
    func showMembers(_ members: [Member])
}
