//
//  TrackerInteractor.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class TrackerInteractor: Interactor {
    
    private let repo: TrackerRepository
    private let group: Group
    private let sprint: Sprint
    
    public init(executors: Executors, repo: TrackerRepository, sprint: Sprint, group: Group) {
        self.repo = repo
        self.group = group
        self.sprint = sprint
        super.init(executors: executors)
    }
    
    public func getSprint() -> Observable<Sprint> {
        return Observable.just(sprint)
    }
    
    func getMembers() -> Observable<[Member]> {
        return applySchedulers(
            repo.getMembers(of: sprint, of: group)
        )
    }
}
