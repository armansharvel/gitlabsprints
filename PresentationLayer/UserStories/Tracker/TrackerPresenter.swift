//
//  TrackerPresenter.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class TrackerPresenter<V: TrackerViewIO>: Presenter<V> {
    
    private struct State {
        let sprint = Variable<Sprint>()
        let members = Variable<[Member]>()
    }
    
    private let interactor: TrackerInteractor
    private let navigator: TrackerNavigator
    private let state = State()
    
    public init(interactor: TrackerInteractor, navigator: TrackerNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    override func setup() {
        interactor.getSprint()
            .subscribe(
                onNext: { [weak self] in self?.state.sprint.setValue($0) }
            )
            .disposed(by: disposeBag!)
        
        interactor.getMembers()
            .trackActivity(self)
            .subscribe(
                onNext: { [weak self] in self?.state.members.setValue($0) },
                onError: { [weak self] in self?.view?.showLoadingError(ErrorWithRecovery("Error: \($0)")) }
            )
            .disposed(by: disposeBag!)
    }
    
    override func viewAttached() {
        guard let viewIO = self.view else { return }
        
        state.sprint.observe { viewIO.setSprintInfo($0) }
        state.members.observe { viewIO.showMembers($0) }
        
        viewIO.backButtonPressed.observe { [weak self] _ in
            self?.navigator.navigateBackToSprints()
        }
    }
}
