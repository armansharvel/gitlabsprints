//
//  DashboardViewIO.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import DomainLayer

/// View of following groups
/// contains a table view of GitLab groups that user is currently following
// sourcery: mock
public protocol DashboardViewIO: ViewIO {
    
    //// Show available groups
    func showGroups(_ groups: [Group])
    
    /// User taps the cell
    /// - Return: group id
    var tableViewDidSelectGroup: Driver<Int> { get }
    
    /// Groups loading error occured
    func showGroupsLoadingError(_ error: ErrorWithRecovery)
}
