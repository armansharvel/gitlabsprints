//
//  DashboardPresenter.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

public class DashboardPresenter<V: DashboardViewIO>: Presenter<V> {
    
    private struct State {
        let groups = Variable<[Group]>()
    }
    
    private let interactor: DashboardInteractor
    private let navigator: DashboardNavigator
    private let state = State()
    
    public init(interactor: DashboardInteractor, navigator: DashboardNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    override func setup() {
        interactor.loadFollowingGroups()
            .trackActivity(self)
            .subscribe(
                onNext: { [weak self] in self?.state.groups.setValue($0) },
                onError: { [weak self] in self?.view?.showGroupsLoadingError(ErrorWithRecovery("Error: \($0)")) }
            )
            .disposed(by: disposeBag!)
    }
    
    override func viewAttached() {
        
        guard let viewIO = self.view else { return }
        
        state.groups.observe { viewIO.showGroups($0) }
        
        viewIO.tableViewDidSelectGroup
            .drive(
                onNext: { [weak self] groupId in
                    guard let group = self?.state.groups.value?.first(where: { $0.uid == groupId }) else { return }
                    self?.navigator.navigateToGroupSprints(group)
            })
            .disposed(by: disposeBag!)
    }
}
