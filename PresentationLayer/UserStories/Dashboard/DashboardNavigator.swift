//
//  DashboardNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer

/// Dashboard navigator
// sourcery: mock
public protocol DashboardNavigator {
    func navigateToGroupSprints(_ group: Group)
}
