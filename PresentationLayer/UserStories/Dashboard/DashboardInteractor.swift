//
//  DashboardInteractor.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

/// Dashboard screen interactor

public class DashboardInteractor: Interactor {
    
    private let repo: GroupsRepository
    
    public init(executors: Executors, repo: GroupsRepository) {
        self.repo = repo
        super.init(executors: executors)
    }
    
    /// Loads the list of groups from UserDefaults that user is currently following
    ///
    /// - Returns: Groups that user is currently following
    func loadFollowingGroups() -> Observable<[Group]> {
        return applySchedulers(
            repo.loadFollowingGroups()
        )
    }    
}
