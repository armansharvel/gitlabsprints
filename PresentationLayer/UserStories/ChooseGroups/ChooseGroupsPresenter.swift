//
//  ChooseGroupsPresenter.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

/// Presents Groups Screen where user is able to choose what groups to track in the app
public class ChooseGroupsPresenter<V: ChooseGroupsViewIO>: Presenter<V> {
    
    private struct State {
        let groups = Variable<[Group]>()
        let selectedGroups = Variable<[Group]>()
    }
    
    private let interactor: ChooseGroupsInteractor
    private let navigator: ChooseGroupsNavigator
    private let state = State()
        
    public init(interactor: ChooseGroupsInteractor, navigator: ChooseGroupsNavigator) {
        self.interactor = interactor
        self.navigator = navigator
    }
    
    override func setup() {
        interactor.listGroups()
            .trackActivity(self)
            .subscribe(
                onNext: { [weak self] in self?.state.groups.setValue($0) },
                onError: { [weak self] in self?.view?.showGroupsLoadingError(ErrorWithRecovery("Error: \($0)")) }
            )
            .disposed(by: disposeBag!)
    }
    
    override func viewAttached() {
        
        guard let viewIO = self.view else { return }
        
        state.groups.observe { viewIO.showGroups($0) }
        
        viewIO.followGroupsButtonClicked.observe { [weak self] _ in
            guard let groups = self?.state.selectedGroups.value else { return }
            if groups.isEmpty { return }
            self?.interactor.updateFollowingGroups(groups: groups)
            self?.navigator.navigateToDashboard()
        }
        
        viewIO.tableViewDidSelectGroup
            .drive(
                onNext: { [weak self] groupId in
                    guard let group = self?.state.groups.value?.first(where: { $0.uid == groupId }) else { return }
                    var groups = self?.state.selectedGroups.value ?? []
                    groups.append(group)
                    self?.state.selectedGroups.setValue(groups)
            })
            .disposed(by: self.disposeBag!)
        
        viewIO.tableViewDidDeselectGroup
            .drive(
                onNext: { [weak self] groupId in
                    guard let group = self?.state.groups.value?.first(where: { $0.uid == groupId }) else { return }
                    let groups = self?.state.selectedGroups.value ?? []
                    self?.state.selectedGroups.setValue(groups.filter { $0.uid != group.uid })
            })
            .disposed(by: self.disposeBag!)
        
    }
}
