//
//  ChooseGroupsNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation

/// Groups screen navigator
// sourcery: mock
public protocol ChooseGroupsNavigator {
    func navigateToDashboard()
}
