//
//  ChooseGroupsViewIO.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import RxSwift
import RxCocoa
import DomainLayer

/// View of groups
/// contains a table view of GitLab groups that user is able to subscribe on
// sourcery: mock
public protocol ChooseGroupsViewIO: ViewIO {
    
    //// Show available groups
    func showGroups(_ groups: [Group])
    
    /// User clicks button to follow chosen groups
    var followGroupsButtonClicked: Signal<Void> { get }
    
    /// User selects group to follow
    /// - Return: group id
    var tableViewDidSelectGroup: Driver<Int> { get }
    
    /// User deselects following group
    /// - Return: group id
    var tableViewDidDeselectGroup: Driver<Int> { get }
    
    /// Groups loading error occured
    func showGroupsLoadingError(_ error: ErrorWithRecovery)
}
