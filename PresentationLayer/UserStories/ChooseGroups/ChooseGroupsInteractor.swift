//
//  ChooseGroupsInteractor.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift
import DomainLayer

/// Groups screen interactor
public class ChooseGroupsInteractor: Interactor {
    
    private let repo: GroupsRepository
    
    public init(executors: Executors, repo: GroupsRepository) {
        self.repo = repo
        super.init(executors: executors)
    }
    
    /// Lists groups to follow
    ///
    /// - Returns: An array of groups that is possible to follow
    func listGroups() -> Observable<[Group]> {
        return applySchedulers(
            repo.listGroups()
        )
    }
    
    /// Updates the list of groups that are followed by user
    func updateFollowingGroups(groups: [Group]) {
        repo.updateFollowingGroups(groups: groups)
    }
}
