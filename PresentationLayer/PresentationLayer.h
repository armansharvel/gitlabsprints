//
//  PresentationLayer.h
//  PresentationLayer
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PresentationLayer.
FOUNDATION_EXPORT double PresentationLayerVersionNumber;

//! Project version string for PresentationLayer.
FOUNDATION_EXPORT const unsigned char PresentationLayerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PresentationLayer/PublicHeader.h>


