//
//  glsprintsTests.swift
//  glsprintsTests
//
//  Created by Andrew Ousenko on 14/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
@testable import glsprints

class glsprintsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testR() {
        do {
            try R.validate()
        } catch {
            print(error)
            XCTFail("R.swift validation failed")
        }
    }

}
