platform :ios, '9.3'
inhibit_all_warnings!
use_frameworks!

def rx_deps
    pod 'RxSwift', '~> 3.6'
    pod 'RxCocoa', '~> 3.6'
end

def network
    pod 'Moya', '~> 8.0.0'
    pod 'MoyaSugar', '~> 0.4'
    pod 'MoyaSugar/RxSwift', '~> 0.4'
    pod 'Moya-Gloss'
    pod 'Moya-Gloss/RxSwift'
end

def authorization
    pod 'OAuthSwift', '~> 1.1.2'
    pod 'Locksmith'
end

def common_pods
    pod 'Reqres', '2.0'
    pod 'Swinject', '2.1'
    pod 'R.swift', '3.3'
end

def common_testing_pods
    pod 'Sourcery', '0.8.0'
end

target 'glsprints' do

  common_pods
  rx_deps

  # Analytics
  pod 'Fabric'
  pod 'Crashlytics'

end

target 'glsprintsTests' do
    common_pods
    rx_deps
end

target 'PresentationLayer' do
    rx_deps
end

target 'PresentationLayerTests' do
    rx_deps
    common_testing_pods
end

target 'DomainLayer' do
    rx_deps
end

target 'DataLayer' do
    rx_deps
    network
    authorization
end

target 'DataLayerTests' do
    rx_deps
    authorization
end
