//
//  PresentationLayerTests.swift
//  PresentationLayerTests
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
@testable import PresentationLayer

class PresentationLayerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testVariable() {
        let const = 42
        var result = 0

        let variable = Variable<Int>()
        variable.observe { result = $0 }
        variable.setValue(const)

        XCTAssert(result == const)
    }

    func testSignal() {
        let const = 42
        var result = 0

        let signal = Signal<Int>()
        signal.observe { result = $0 }
        signal.send(const)

        XCTAssert(result == const)
    }

}
