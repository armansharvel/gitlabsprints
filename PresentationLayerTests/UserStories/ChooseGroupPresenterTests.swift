//
//  ChooseGroupPresenterTests.swift
//  glsprints
//
//  Created by Arman Arutyunov on 01.11.2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
import DomainLayer
import RxSwift
import Foundation

@testable import PresentationLayer

class ChooseGroupPresenterTests: XCTestCase {

    // Setup
    let groups = [Group(uid: 1, name: "test1", description: ""), Group(uid: 2, name: "test2", description: "")]
    var interactor: ChooseGroupsInteractor!
    var view: MockChooseGroupsViewIO!
    var sut: ChooseGroupsPresenter<MockChooseGroupsViewIO>!

    // Inputs
    let groupsInput = PublishSubject<[Group]>()
    let groupSelectionInput = PublishSubject<Int>()
    let groupDeselectionInput = PublishSubject<Int>()
    let groupsConfirmedInput = Signal<Void>()

    // Outputs
    var showedGroups = [Group]()
    var showedErrors = [ErrorWithRecovery]()
    var isLoadingSeq = [Bool]()

    var followedGroups = [Group]()
    var navigatedNext = false

    override func setUp() {
        super.setUp()

        cleanup()
        setupDeps()
    }

    func testGroupsLoading() {
        sut.attachView(view: view)

        // Simulate groups loading
        groupsInput.onNext(groups)
        groupsInput.onCompleted()
        
        XCTAssert(showedGroups == groups)
        XCTAssert(isLoadingSeq == [false, true, false])
    }

    func testGroupsLoadingError() {
        sut.attachView(view: view)

        // Throw error from repository
        groupsInput.onError(NSError(domain: "", code: 0, userInfo: nil))

        XCTAssert(showedGroups == [])
        XCTAssert(showedErrors.count == 1)
        XCTAssert(isLoadingSeq == [false, true, false])
    }

    func testGroupsSelection() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        groupsInput.onNext(groups)
        groupsInput.onCompleted()

        // Select first group
        let group = groups.first!
        groupSelectionInput.onNext(group.uid)

        // followedGroups should be empty before confirmation
        XCTAssert(followedGroups.isEmpty)

        // sending confirmation signal
        groupsConfirmedInput.send(())

        XCTAssert(followedGroups.first! == group)
    }

    func testGroupsDeselection() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        groupsInput.onNext(groups)
        groupsInput.onCompleted()

        // Select first group
        groupSelectionInput.onNext(groups[0].uid)

        // Select second group
        groupSelectionInput.onNext(groups[1].uid)

        /// Deselect first group
        groupDeselectionInput.onNext(groups[0].uid)

        // sending confirmation signal
        groupsConfirmedInput.send(())

        XCTAssert(followedGroups.first! == groups[1])
    }

    func testGroupsFollowed() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        groupsInput.onNext(groups)
        groupsInput.onCompleted()

        // Select first group
        groupSelectionInput.onNext(groups[0].uid)

        // sending confirmation signal
        groupsConfirmedInput.send(())

        XCTAssert(navigatedNext)
    }

    func testGroupsNotFollowed() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        groupsInput.onNext(groups)
        groupsInput.onCompleted()

        // sending confirmation signal
        groupsConfirmedInput.send(())

        // Navigation with empty groups to follow should be disabled
        XCTAssertFalse(navigatedNext)
    }
}

extension ChooseGroupPresenterTests {
    
    fileprivate func cleanup() {
        showedGroups = [Group]()
        showedErrors = [ErrorWithRecovery]()
        isLoadingSeq = [Bool]()
        followedGroups = [Group]()
        navigatedNext = false
    }
    
    fileprivate func setupDeps() {
        setupViewIO()
        interactor = ChooseGroupsInteractor(executors: TestBlockingExecutors(),
                                            repo: getRepo())
        sut = ChooseGroupsPresenter(interactor: interactor,
                                    navigator: getNavigator())
    }
    
    private func setupViewIO() {
        view = MockChooseGroupsViewIO()
        view._showGroups = { [weak self] in self?.showedGroups = $0 }
        view._followGroupsButtonClicked = groupsConfirmedInput
        view._tableViewDidSelectGroup = groupSelectionInput.asDriver(onErrorDriveWith: .never())
        view._tableViewDidDeselectGroup = groupDeselectionInput.asDriver(onErrorDriveWith: .never())
        view._showGroupsLoadingError = { [weak self] in self?.showedErrors.append($0) }
        view._show = { [weak self] in self?.isLoadingSeq.append($0) }
    }
    
    private func getRepo() -> MockGroupsRepository {
        let repo = MockGroupsRepository()
        repo._listGroups = groupsInput
        repo._updateFollowingGroups = { [weak self] in self?.followedGroups = $0 }
        return repo
    }
    
    private func getNavigator() -> MockChooseGroupsNavigator {
        let navigator = MockChooseGroupsNavigator()
        navigator._navigateToDashboard = { [weak self] in self?.navigatedNext = true }
        return navigator
    }
    
}
