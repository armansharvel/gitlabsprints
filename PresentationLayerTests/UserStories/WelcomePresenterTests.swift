//
//  WelcomePresenterTests.swift
//  glsprints
//
//  Created by Arman Arutyunov on 01/11/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
import DomainLayer
import RxSwift
import Foundation

@testable import PresentationLayer

class WelcomePresenterTests: XCTestCase {
    
    // Setup
    let groups = [Group(uid: 1, name: "test1", description: ""), Group(uid: 2, name: "test2", description: "")]
    var interactor: WelcomeInteractor!
    var view: MockWelcomeViewIO!
    var sut: WelcomePresenter<MockWelcomeViewIO>!
    
    // Inputs
    let groupsInput = PublishSubject<[Group]>()
    let isSignedIn = PublishSubject<Bool>()
    let logInClicked = Signal<Void>()
    let followingGroups = PublishSubject<[Group]>()
    
    // Outputs
    var followedGroups = [Group]()
    var navigatedToAuth = false
    var navigatedToGroupSelection = false
    var navigatedToDashboard = false
    
    override func setUp() {
        super.setUp()
        
        cleanup()
        setupDeps()
    }
    
    func testDidNavigateToAuth() {
        sut.attachView(view: view)
        
        followingGroups.onNext([])
        isSignedIn.onNext(false)
        logInClicked.send()
        
        XCTAssert(navigatedToAuth)
    }
    
    func testDidNavigateToGroupSelection() {
        sut.attachView(view: view)
        
        followingGroups.onNext([])
        isSignedIn.onNext(true)
        
        XCTAssert(navigatedToGroupSelection)
    }
    
    func testDidNavigateToDashboard() {
        sut.attachView(view: view)
        
        followingGroups.onNext(groups)
        isSignedIn.onNext(true)
        
        XCTAssert(navigatedToDashboard)
    }
    
}

extension WelcomePresenterTests {
    
    fileprivate func cleanup() {
        followedGroups = [Group]()
        navigatedToAuth = false
        navigatedToGroupSelection = false
        navigatedToDashboard = false
    }
    
    fileprivate func setupDeps() {
        setupViewIO()
        interactor = WelcomeInteractor(executors: TestBlockingExecutors(),
                                       authRepo: getAuthRepo(),
                                       groupsRepo: getGroupsRepo())
        sut = WelcomePresenter(interactor: interactor,
                               navigator: getNavigator())
    }
    
    private func setupViewIO() {
        view = MockWelcomeViewIO()
        view._logInClicked = logInClicked
        view._show = { _ in }
    }
    
    private func getAuthRepo() -> MockAuthRepository {
        let authRepo = MockAuthRepository()
        authRepo._signIn = Observable.just("")
        authRepo._isSignedIn = isSignedIn
        return authRepo
    }
    
    private func getGroupsRepo() -> MockGroupsRepository {
        let groupsRepo = MockGroupsRepository()
        groupsRepo._listGroups = groupsInput
        groupsRepo._updateFollowingGroups = { [weak self] in self?.followedGroups = $0 }
        groupsRepo._loadFollowingGroups = followingGroups
        return groupsRepo
    }
    
    private func getNavigator() -> MockWelcomeNavigator {
        let navigator = MockWelcomeNavigator()
        navigator._navigateToAuth = { [weak self] in self?.navigatedToAuth = true }
        navigator._navigateToDashboard = { [weak self] in self?.navigatedToDashboard = true }
        navigator._navigateToGroupsSelection = { [weak self] in self?.navigatedToGroupSelection = true }
        return navigator
    }
    
}
