//
//  AuthPresenterTests.swift
//  glsprints
//
//  Created by Arman Arutyunov on 01/11/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
import DomainLayer
import RxSwift

@testable import PresentationLayer

class AuthPresenterTests: XCTestCase {
    
    // Setup
    var authRepo: MockAuthRepository!
    var interactor: AuthInteractor!
    var view: MockAuthViewIO!
    var sut: AuthPresenter<MockAuthViewIO>!
    
    // Inputs
    let signInReq = PublishSubject<String>()
    let isSignedIn = PublishSubject<Bool>()
    let username = Signal<String>()
    let password = Signal<String>()
    let logInClicked = Signal<Void>()
    
    // Outputs
    var showedErrors = [ErrorWithRecovery]()
    var isLoadingSeq = [Bool]()
    var navigatedToGroupSelection = false
    
    override func setUp() {
        super.setUp()

        cleanup()
        setupDeps()
    }
    
    func testAuthorizationDidntStartWithEmptyCredentials() {
        sut.attachView(view: view)
        
        logInClicked.send()
        signInReq.onNext("")

        XCTAssertFalse(navigatedToGroupSelection)
    }
    
    func testAuthorizationDidntStartWithEmptyUsername() {
        sut.attachView(view: view)
        
        password.send("password")
        logInClicked.send()
        signInReq.onNext("")

        XCTAssertFalse(navigatedToGroupSelection)
    }
    
    func testAuthorizationDidntStartWithEmptyPassword() {
        sut.attachView(view: view)
        
        username.send("username")
        logInClicked.send()
        signInReq.onNext("")

        XCTAssertFalse(navigatedToGroupSelection)
    }
    
    func testAuthorizationDidStartWithFullCredentials() {
        sut.attachView(view: view)
        
        username.send("username")
        password.send("password")
        logInClicked.send()
        signInReq.onNext("")
        signInReq.onCompleted()

        XCTAssertTrue(navigatedToGroupSelection)
        XCTAssert(isLoadingSeq == [false, true, false])
    }

    func testAuthorizationDidThrowError() {
        sut.attachView(view: view)

        // Fill credentials and confirm
        username.send("username")
        password.send("password")
        logInClicked.send()

        // Throw an error
        signInReq.onError(NSError(domain: "", code: 0, userInfo: nil))
        XCTAssert(showedErrors.count > 0)
        XCTAssert(isLoadingSeq == [false, true, false])

        // No navigation should happen
        XCTAssertFalse(navigatedToGroupSelection)
    }

    func testAuthorizationDidRecoverAfterError() {
        sut.attachView(view: view)

        // Fill credentials and confirm
        username.send("username")
        password.send("password")
        logInClicked.send()

        // Throw an error
        signInReq.onError(NSError(domain: "", code: 0, userInfo: nil))

        // Recover and try again
        authRepo._signIn = Observable.just("")
        logInClicked.send()

        // Should be succedeed
        XCTAssertTrue(navigatedToGroupSelection)
    }
}

extension AuthPresenterTests {

    fileprivate func cleanup() {
        showedErrors = [ErrorWithRecovery]()
        isLoadingSeq = [Bool]()
        navigatedToGroupSelection = false
    }

    fileprivate func setupDeps() {
        setupViewIO()
        setupAuthRepo()
        interactor = AuthInteractor(executors: TestBlockingExecutors(),
                                    repo: authRepo)
        sut = AuthPresenter(interactor: interactor,
                            navigator: getNavigator())
    }

    private func setupViewIO() {
        view = MockAuthViewIO()
        view._username = username
        view._password = password
        view._logInClicked = logInClicked
        view._showLoginError = { [weak self] in self?.showedErrors.append($0) }
        view._show = { [weak self] in self?.isLoadingSeq.append($0) }
    }

    private func setupAuthRepo(){
        authRepo = MockAuthRepository()
        authRepo._signIn = signInReq
        authRepo._isSignedIn = isSignedIn
    }

    private func getNavigator() -> MockAuthNavigator {
        let navigator = MockAuthNavigator()
        navigator._navigateToGroupSelection = { [weak self] in self?.navigatedToGroupSelection = true }
        return navigator
    }

}
