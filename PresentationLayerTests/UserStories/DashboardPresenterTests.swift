//
//  DashboardPresenterTests.swift
//  glsprints
//
//  Created by Arman Arutyunov on 02/11/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import XCTest
import DomainLayer
import RxSwift
import Foundation

@testable import PresentationLayer

class DashboardPresenterTests: XCTestCase {
    
    // Setup
    let groups = [Group(uid: 1, name: "test1", description: ""), Group(uid: 2, name: "test2", description: "")]
    var interactor: DashboardInteractor!
    var view: MockDashboardViewIO!
    var sut: DashboardPresenter<MockDashboardViewIO>!
    
    // Inputs
    let followingGroupsInput = PublishSubject<[Group]>()
    let groupSelectionInput = PublishSubject<Int>()
    
    // Outputs
    var showedGroups = [Group]()
    var showedErrors = [ErrorWithRecovery]()
    var isLoadingSeq = [Bool]()
    var destinationGroup = Group(uid: 99, name: "Bad Group", description: "Bad as hell group")
    
    override func setUp() {
        super.setUp()
        
        cleanup()
        setupDeps()
    }
    
    func testGroupsLoading() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        followingGroupsInput.onNext(groups)
        followingGroupsInput.onCompleted()
        
        XCTAssert(showedGroups == groups)
        XCTAssert(isLoadingSeq == [false, true, false])
    }
    
    func testGroupsLoadingError() {
        sut.attachView(view: view)
        
        // Throw error from repository
        followingGroupsInput.onError(NSError(domain: "", code: 0, userInfo: nil))
        
        XCTAssert(showedGroups == [])
        XCTAssert(showedErrors.count == 1)
        XCTAssert(isLoadingSeq == [false, true, false])
    }
    
    func testGroupsSelection() {
        sut.attachView(view: view)
        
        // Simulate groups loading
        followingGroupsInput.onNext(groups)
        followingGroupsInput.onCompleted()        
        
        // Select first group
        let group = groups.first!
        groupSelectionInput.onNext(group.uid)
        
        // destination group should the same we selected
        XCTAssert(destinationGroup == group)
    }
    
}

extension DashboardPresenterTests {
    
    fileprivate func cleanup() {
        showedGroups = [Group]()
        showedErrors = [ErrorWithRecovery]()
        isLoadingSeq = [Bool]()
        destinationGroup = Group(uid: 99, name: "Bad Group", description: "Bad as hell group")
    }
    
    fileprivate func setupDeps() {
        setupViewIO()
        interactor = DashboardInteractor(executors: TestBlockingExecutors(),
                                         repo: getRepo())
        sut = DashboardPresenter(interactor: interactor,
                                 navigator: getNavigator())
    }
    
    private func setupViewIO() {
        view = MockDashboardViewIO()
        view._showGroups = { [weak self] in self?.showedGroups = $0 }
        view._tableViewDidSelectGroup = groupSelectionInput.asDriver(onErrorDriveWith: .never())
        view._showGroupsLoadingError = { [weak self] in self?.showedErrors.append($0) }
        view._show = { [weak self] in self?.isLoadingSeq.append($0) }
    }
    
    private func getRepo() -> MockGroupsRepository {
        let repo = MockGroupsRepository()
        repo._loadFollowingGroups = followingGroupsInput
        return repo
    }
    
    private func getNavigator() -> MockDashboardNavigator {
        let navigator = MockDashboardNavigator()
        navigator._navigateToGroupSprints = { [weak self] in self?.destinationGroup = $0 }
        return navigator
    }
    
}
