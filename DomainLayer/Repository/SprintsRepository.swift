//
//  SprintsRepository.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift

public protocol SprintsRepository {
    
    /// Lists current sprints
    ///
    /// - Returns: An array of current sprints
    func listSprints(ofGroup group: Group) -> Observable<[Sprint]>
    
}
