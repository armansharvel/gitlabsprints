//
//  AuthRepository.swift
//  glsprints
//
//  Created by Andrew Ousenko on 26/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift

// sourcery: mock
public protocol AuthRepository {
    
    /// Sign into Git Lab
    ///
    /// - Parameter email: user's email
    /// - Parameter password: user's password
    /// - Returns: Gitlab token
    func signIn(email: String, password: String) -> Observable<String>

    
    /// Whether user is signed into GitLab
    ///
    /// - Returns: emits true if user is signed into GitLab
    func isSignedIn() -> Observable<Bool>
}
