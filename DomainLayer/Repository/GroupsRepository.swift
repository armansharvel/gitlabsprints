//
//  GroupRepository.swift
//  glsprints
//
//  Created by Andrew Ousenko on 26/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift

// sourcery: mock
public protocol GroupsRepository {
    
    /// Lists groups to follow
    ///
    /// - Returns: An array of groups that is possible to follow
    func listGroups() -> Observable<[Group]>
    
    /// Updates the list of groups that are followed by user
    func updateFollowingGroups(groups: [Group])

    /// Loads the list of groups from UserDefaults that user is currently following
    ///
    /// - Returns: Groups that user is currently following
    func loadFollowingGroups() -> Observable<[Group]>    
}
