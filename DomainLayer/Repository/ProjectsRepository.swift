//
//  ProjectsRepository.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift

public protocol ProjectsRepository {
    func listProjects(of sprint: Sprint) -> Observable<[Project]>
    
    func listGoals(of sprint: Sprint) -> Observable<[Issue]>
}
