//
//  TrackerRepository.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift

public protocol TrackerRepository {
    
    func getMembers(of sprint: Sprint, of group: Group) -> Observable<[Member]>
}
