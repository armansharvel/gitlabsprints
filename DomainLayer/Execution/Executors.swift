//
//  Executors.swift
//  glsprints
//
//  Created by Andrew Ousenko on 17/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import RxSwift


/// Executors to be used throughout the application, in Interactors
public protocol Executors {
    
    /// Scheduler used for doing the heavy job
    var worker: ImmediateSchedulerType { get }
    
    /// Scheduler used for receiving notifications from Observable
    var notifier: ImmediateSchedulerType { get }

}


/// Standard executors to be used through app
public class StandardExecutors: Executors {

    public init() {}
    
    public var notifier: ImmediateSchedulerType {
        return MainScheduler.instance
    }
    
    public var  worker: ImmediateSchedulerType {
        return SerialDispatchQueueScheduler(internalSerialQueueName: "serialQueue")
    }
    
}



public class TestBlockingExecutors : Executors{

    public init() {}
    
    public var notifier: ImmediateSchedulerType {
        return CurrentThreadScheduler.instance
    }
    
    public var  worker: ImmediateSchedulerType {
        return CurrentThreadScheduler.instance
    }
    
    
}
