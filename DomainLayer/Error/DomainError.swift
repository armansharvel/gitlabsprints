//
//  File.swift
//  glsprints
//
//  Created by Andrew Ousenko on 18/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public enum DomainError: Error {
    case notImplemented
}

public enum NetworkError: Error {
    case badResponse
}
