//
//  Issue.swift
//  glsprints
//
//  Created by Arman Arutyunov on 14.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct Issue {
    public enum State {
        case open
        case closed
    }
    public let id: Int
    public let projectId: Int
    public let title: String
    public let avatarLetter: String
    public let description: String
    public let state: State
    public let assigneeId: Int
    public let assigneeName: String
    public let timeStats: TimeStats
    public let isGoal: Bool
    
    public init(id: Int, projectId: Int, title: String, description: String, state: String, assigneeId: Int, assigneeName: String, timeStats: TimeStats, isGoal: Bool) {
        self.id = id
        self.projectId = projectId
        self.title = title
        self.description = description
        self.state = state == "closed" ? .closed : .open
        self.isGoal = isGoal
        self.assigneeId = assigneeId
        self.assigneeName = assigneeName
        self.timeStats = timeStats
        if let firstLetter = title.first {
            self.avatarLetter = String(firstLetter).uppercased()
        } else {
            self.avatarLetter = ""
        }
    }
}
