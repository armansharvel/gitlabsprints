//
//  Profile.swift
//  glsprints
//
//  Created by Andrew Ousenko on 26/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation



/// Gitlab User profile
public struct Profile {
    
    let name: String

    public init (_ name: String){
        self.name = name
    }
    
}
