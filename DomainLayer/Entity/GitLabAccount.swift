//
//  GitLabAccount.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/12/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct GitLabAccount {
    public let accessToken: String
    public let tokenType: String
    public let grantDate: Date
    
    public init(accessToken: String, tokenType: String, grantDate: Date) {
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.grantDate = grantDate
    }
}
