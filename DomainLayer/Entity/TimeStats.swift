//
//  TimeStats.swift
//  glsprints
//
//  Created by Arman Arutyunov on 10/2/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct TimeStats {
    
    public let estimated: Int
    public let spent: Int
    
    public init(estimated: Int, spent: Int) {
        self.estimated = estimated
        self.spent = spent
    }
}
