//
//  Sprint.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/21/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct Sprint {
    public let uid: Int
    public let title: String
    public let startDate: Date
    public let dueDate: Date?
    public let issues: [Issue]
    public let completion: Float
    
    public init(uid: Int, title: String, startDate: Date, dueDate: Date?, issues: [Issue]) {
        
        func completionPercentage(issues: [Issue]) -> Float {
            let totalIssuesCount = Float(issues.count)
            guard totalIssuesCount != 0 else { return 0 }
            var closedIssues: Float = 0
            for issue in issues {
                if issue.state == .closed {
                    closedIssues += 1
                }
            }
            return (closedIssues/totalIssuesCount)*100
        }
        
        self.uid = uid
        self.title = title
        self.startDate = startDate
        self.dueDate = dueDate
        self.issues = issues
        self.completion = completionPercentage(issues: issues)
    }
    
}
