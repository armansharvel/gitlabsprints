//
//  Identifier.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation

public struct Identifier<T>: RawRepresentable, Hashable, Equatable {
    public let rawValue: Int
    public init(rawValue: Int) { self.rawValue = rawValue }
    public var hashValue: Int { return rawValue.hashValue }
}
