//
//  Group.swift
//  glsprints
//
//  Created by Arman Arutyunov on 14.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct Group {
    public let uid: Int
    public let name: String
    public let description: String
    public let avatarLetter: String
    
    public init(uid: Int, name: String, description: String) {
        self.uid = uid
        self.name = name
        self.description = description
        if let firstLetter = name.first {
            self.avatarLetter = String(firstLetter).uppercased()
        } else {
            self.avatarLetter = ""
        }
    }
}

extension Group: Equatable {
    public static func ==(lhs: Group, rhs: Group) -> Bool {
        return lhs.uid == rhs.uid && lhs.name == rhs.name
    }
}
