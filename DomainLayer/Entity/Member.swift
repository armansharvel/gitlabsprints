//
//  Member.swift
//  glsprints
//
//  Created by Arman Arutyunov on 10/2/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct Member {
    public let id: Int
    public let username: String
    public let avatarLetter: String
    public var timeStats: MemberTimeStats
    
    public init(id: Int, username: String, timeStats: MemberTimeStats) {
        self.id = id
        self.username = username
        self.timeStats = timeStats
        if let firstLetter = username.first {
            self.avatarLetter = String(firstLetter).uppercased()
        } else {
            self.avatarLetter = ""
        }
    }
}
