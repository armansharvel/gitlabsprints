//
//  Project.swift
//  glsprints
//
//  Created by Arman Arutyunov on 14.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct Project {
    
    public let uid: Int
    public let name: String
    public let issues: [Issue]
    public let completion: Float
    
    public init(uid: Int, name: String, issues: [Issue]) {
        
        func completionPercentage(issues: [Issue]) -> Float {
            let totalIssuesCount = Float(issues.count)
            guard totalIssuesCount != 0 else { return 0 }
            var closedIssues: Float = 0
            for issue in issues {
                if issue.state == .closed {
                    closedIssues += 1
                }
            }
            return (closedIssues/totalIssuesCount)*100
        }
        
        self.uid = uid
        self.name = name
        self.issues = issues
        self.completion = completionPercentage(issues: issues)
    }
    
    public func getIssues(open: Bool) -> [Issue] {
        var openIssues = [Issue]()
        var closedIssues = [Issue]()
        for issue in issues {
            issue.state == .open ? openIssues.append(issue) : closedIssues.append(issue)
        }
        return open ? openIssues : closedIssues
    }
}
