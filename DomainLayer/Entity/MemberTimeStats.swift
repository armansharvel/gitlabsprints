//
//  TimeStats.swift
//  glsprints
//
//  Created by Arman Arutyunov on 10/2/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct MemberTimeStats {
    
    public init() {}
    
    public var closedIssuesAmount = 0
    public var openIssuesAmount = 0
    public var closedEstimatedTime = 0
    public var openEstimatedTime = 0
    public var closedSpentTime = 0
    public var openSpentTime = 0
}
