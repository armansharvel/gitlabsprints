//
//  WelcomeSceneContainer.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

/// Welcome scene / DI container builder
class WelcomeScene: Scene {
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(WelcomeInteractor.self) { r in
                WelcomeInteractor(executors: r.resolve(Executors.self)!,
                                  authRepo: r.resolve(AuthRepository.self)!,
                                  groupsRepo: r.resolve(GroupsRepository.self)!)
            }
            container.register(WelcomePresenter.self) { r -> WelcomePresenter<WelcomeViewController> in
                WelcomePresenter(interactor: r.resolve(WelcomeInteractor.self)!,
                                 navigator: r.resolve(WelcomeNavigator.self)!)
            }
            container.register(WelcomeNavigator.self) { _ in
                WelcomeSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = WelcomeViewController()
                vc.presenter = r.resolve(WelcomePresenter.self)
                return vc
            }
        }
    }
}
