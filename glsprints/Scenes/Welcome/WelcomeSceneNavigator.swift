//
//  WelcomeSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 25.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer

class WelcomeSceneNavigator: BaseNavigator, WelcomeNavigator {
    func navigateToAuth() {
        scene.setFirst(AuthScene())
    }

    func navigateToGroupsSelection() {
        scene.setFirst(ChooseGroupsScene(), animated: false)
    }

    func navigateToDashboard() {
        scene.setFirst(DashboardScene(), animated: false)
    }
}
