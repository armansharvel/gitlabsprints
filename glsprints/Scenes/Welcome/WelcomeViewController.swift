//
//  WelcomeViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 21.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import PresentationLayer
import Rswift

class WelcomeViewController: ViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var welcomeLabel: UILabel!

    var presenter: WelcomePresenter<WelcomeViewController>?

    override func viewDidLoad() {
        super.viewDidLoad()

        button.setTitle(R.string.localizable.login_button(), for: .normal)
        welcomeLabel.attributedText = R.string.localizable.login_title().titleStyled + R.string.localizable.login_subtitle().subtitleStyled

        presenter?.attachView(view: self)
    }
}

extension WelcomeViewController: WelcomeViewIO {
    /// User clicks login button
    var logInClicked: Signal<Void> {
        return button.tapSignal
    }
}
