//
//  AuthSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 25.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer

class AuthSceneNavigator: BaseNavigator, AuthNavigator {
    func navigateToGroupSelection() {
        scene.setFirst(ChooseGroupsScene())
    }
}
