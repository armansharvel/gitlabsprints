//
//  AuthViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 21.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import PresentationLayer
import Rswift

class AuthViewController: ViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!

    var presenter: AuthPresenter<AuthViewController>?

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.attachView(view: self)
    }
}

extension AuthViewController: AuthViewIO {
    /// Username input
    var username: Signal<String> {
        return loginField.textSignal
    }

    /// Password input
    var password: Signal<String> {
        return passwordField.textSignal
    }

    /// User clicks login button
    var logInClicked: Signal<Void> {
        return signInButton.tapSignal
    }

    /// Login error occured
    func showLoginError(_ error: ErrorWithRecovery) {
        showError(error)
    }
}
