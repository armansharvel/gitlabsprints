//
//  AuthSceneContainer.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

/// Auth scene / DI container builder
class AuthScene: Scene {
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(AuthInteractor.self) { r in
                AuthInteractor(executors: r.resolve(Executors.self)!,
                                  repo: r.resolve(AuthRepository.self)!)
            }
            container.register(AuthPresenter.self) { r -> AuthPresenter<AuthViewController> in
                AuthPresenter(interactor: r.resolve(AuthInteractor.self)!,
                                 navigator: r.resolve(AuthNavigator.self)!)
            }
            container.register(AuthNavigator.self) { _ in
                AuthSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = AuthViewController()
                vc.presenter = r.resolve(AuthPresenter.self)
                return vc
            }
        }
    }
}
