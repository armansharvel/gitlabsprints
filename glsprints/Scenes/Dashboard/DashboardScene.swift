//
//  DashboardScene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

/// Groups scene / DI container builder
class DashboardScene: Scene {
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(DashboardInteractor.self) { r in
                DashboardInteractor(executors: r.resolve(Executors.self)!,
                                    repo: r.resolve(GroupsRepository.self)!)
            }
            container.register(DashboardPresenter.self) { r -> DashboardPresenter<DashboardViewController> in
                DashboardPresenter(interactor: r.resolve(DashboardInteractor.self)!,
                                   navigator: r.resolve(DashboardNavigator.self)!)
            }
            container.register(DashboardNavigator.self) { _ in
                DashboardSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = DashboardViewController()
                vc.presenter = r.resolve(DashboardPresenter.self)
                return vc
            }
        }
    }
}
