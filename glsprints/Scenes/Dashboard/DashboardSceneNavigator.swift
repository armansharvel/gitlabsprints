//
//  DashboardSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer
import DomainLayer

class DashboardSceneNavigator: BaseNavigator, DashboardNavigator {
    public func navigateToGroupSprints(_ group: Group) {
        scene.push(SprintsScene(group: group))
    }
}
