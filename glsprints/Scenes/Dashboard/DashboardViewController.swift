//
//  DasboardViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Rswift
import PresentationLayer
import DomainLayer

class DashboardViewController: ViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var groups = PublishSubject<[Group]>()
    
    var presenter: DashboardPresenter<DashboardViewController>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter?.attachView(view: self)
    }    
}

extension DashboardViewController: DashboardViewIO {
    
    //// Show available groups
    func showGroups(_ groups: [Group]) {
        self.groups.onNext(groups)
    }
    
    /// User taps the cell
    var tableViewDidSelectGroup: Driver<Int> {
        return tableView.rx.itemSelected
            .map { [unowned self] indexPath -> Group in try self.tableView.rx.model(at: indexPath) }
            .map { $0.uid }
            .asDriver(onErrorDriveWith: .never())
    }
    
    /// Loading error occured
    func showGroupsLoadingError(_ error: ErrorWithRecovery) {
        showError(error)
    }
}

// MARK: - UI Setup
extension DashboardViewController {
    
    fileprivate func setupSubviews() {
        setupLabel()
        setupTableView()
    }
    
    private func setupLabel() {
        titleLabel.attributedText = R.string.localizable.dashboard_title().localized.titleStyled + R.string.localizable.dashboard_subtitle().subtitleStyled
        titleLabelHeight.constant = titleLabel.heightThatFits
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "GroupCell", bundle: nil), forCellReuseIdentifier: "GroupCell")
        groups
            .bind(to: tableView.rx.items(cellIdentifier: "GroupCell", cellType: GroupCell.self)) { (row, element, cell) in
                cell.setupCellWithGroup(element)
            }
            .disposed(by: disposeBag)
    }
}
