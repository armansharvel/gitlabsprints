//
//  TrackerScene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

class TrackerScene: Scene {
    
    let group: Group
    let sprint: Sprint
    init(group: Group, sprint: Sprint) {
        self.group = group
        self.sprint = sprint
    }
    
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(TrackerInteractor.self) { r in
                TrackerInteractor(executors: r.resolve(Executors.self)!,
                                  repo: r.resolve(TrackerRepository.self)!,
                                  sprint: self.sprint,
                                  group: self.group)
            }
            container.register(TrackerPresenter.self) { r -> TrackerPresenter<TrackerViewController> in
                TrackerPresenter(interactor: r.resolve(TrackerInteractor.self)!,
                                 navigator: r.resolve(TrackerNavigator.self)!)
            }
            container.register(TrackerNavigator.self) { _ in
                TrackerSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = TrackerViewController()
                vc.presenter = r.resolve(TrackerPresenter.self)
                return vc
            }
        }
    }
}
