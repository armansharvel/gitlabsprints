//
//  TrackerSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer

class TrackerSceneNavigator: BaseNavigator, TrackerNavigator {
    
    func navigateBackToSprints() {
        self.scene.pop()
    }
}
