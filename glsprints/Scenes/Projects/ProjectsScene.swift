//
//  ProjectsScene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

class ProjectsScene: Scene {
    
    let sprint: Sprint
    let group: Group
    init(sprint: Sprint, group: Group) {
        self.sprint = sprint
        self.group = group
    }
    
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(ProjectsInteractor.self) { r in
                ProjectsInteractor(executors: r.resolve(Executors.self)!,
                                   repo: r.resolve(ProjectsRepository.self)!,
                                   sprint: self.sprint, group: self.group)
            }
            container.register(ProjectsPresenter.self) { r -> ProjectsPresenter<ProjectsViewController> in
                ProjectsPresenter(interactor: r.resolve(ProjectsInteractor.self)!,
                                  navigator: r.resolve(ProjectsNavigator.self)!)
            }
            container.register(ProjectsNavigator.self) { _ in
                ProjectsSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = ProjectsViewController()
                vc.presenter = r.resolve(ProjectsPresenter.self)
                return vc
            }
        }
    }
}
