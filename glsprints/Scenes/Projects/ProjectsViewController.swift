//
//  ProjectsViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 21.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PresentationLayer
import DomainLayer

class ProjectsViewController: ViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var trackerButton: UIButton!
    @IBOutlet weak var datesLabel: UILabel!
    @IBOutlet weak var groupProjectsTitleLabel: UILabel!
    @IBOutlet weak var groupProjectsTableView: UITableView!
    @IBOutlet weak var groupProjectsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sprintGoalsTitleLabel: UILabel!
    @IBOutlet weak var sprintGoalsTableView: UITableView!
    @IBOutlet weak var sprintGoalsTableViewHeight: NSLayoutConstraint!    
    
    fileprivate var sprint = PublishSubject<Sprint>()
    fileprivate var projects = PublishSubject<[Project]>()
    fileprivate var goals = PublishSubject<[Issue]>()
    
    var presenter: ProjectsPresenter<ProjectsViewController>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSubviews()
        presenter?.attachView(view: self)
    }
}

extension ProjectsViewController: ProjectsViewIO {
    
    var backButtonPressed: Signal<Void> {
        return backButton.tapSignal
    }
    
    var trackerButtonPressed: Signal<Void> {
        return trackerButton.tapSignal
    }
    
    func showLoadingError(_ error: ErrorWithRecovery) {
        showError(error)
    }
    
    func showSprintInfo(_ sprint: Sprint) {
        self.sprint.onNext(sprint)
    }
    
    func showProjects(_ projects: [Project]) {
        self.projects.onNext(projects)
        groupProjectsTableViewHeight.constant = groupProjectsTableView.rowHeight * CGFloat(projects.count)
        UIView.animate(withDuration: 0.3) { 
            self.view.layoutIfNeeded()
        }
    }
    
    func showGoals(_ goals: [Issue]) {
        self.goals.onNext(goals)
        sprintGoalsTableViewHeight.constant = sprintGoalsTableView.rowHeight * CGFloat(goals.count)
        if goals.count > 0 {
            sprintGoalsTitleLabel.isHidden = false
        }
    }
    
}

// MARK: - UI Setup
extension ProjectsViewController {
    
    fileprivate func setupSubviews() {
        sprintGoalsTitleLabel.isHidden = true
        setupLabels()
        setupGroupProjectsTableView()
        setupSprintGoalsTableView()
    }
    
    private func setupLabels() {
        sprint
            .subscribe(
                onNext: { [weak self] sprint in
                    self?.setupLabels(with: sprint)
            })
            .disposed(by: disposeBag)
    }
    
    private func setupLabels(with sprint: Sprint) {
        titleLabel.attributedText = sprint.title.titleStyled
        setupSubtitle(with: sprint)
    }
    
    private func setupSubtitle(with sprint: Sprint) {
        let dueDate: String
        let expiredString: String
        if let date = sprint.dueDate {
            dueDate = " - \(date.toString())"
            expiredString = date > Date() ? "" : " (expired)"
        } else {
            dueDate = ""
            expiredString = ""
        }
        let startDate = sprint.startDate.toString()
        datesLabel.attributedText = startDate.descriptionStyled + dueDate.descriptionStyled + expiredString.subtitleStyled
    }
    
    private func setupGroupProjectsTableView() {
        groupProjectsTableView.register(UINib(nibName: "SprintCell", bundle: nil),
                                        forCellReuseIdentifier: "SprintCell")
        projects
            .bind(to: groupProjectsTableView.rx.items(cellIdentifier: "SprintCell", cellType: SprintCell.self)) { row, element, cell in
                cell.setupCellWithProject(element)
            }
            .disposed(by: disposeBag)
    }
    
    private func setupSprintGoalsTableView() {
        sprintGoalsTableView.register(UINib(nibName: "GroupCell", bundle: nil),
                                      forCellReuseIdentifier: "GroupCell")
        goals
            .bind(to: sprintGoalsTableView.rx.items(cellIdentifier: "GroupCell", cellType: GroupCell.self)) { row, element, cell in
                cell.setupCellWithGoal(element)
            }
            .disposed(by: disposeBag)
        
    }
    
}
