//
//  ProjectsSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer
import DomainLayer

class ProjectsSceneNavigator: BaseNavigator, ProjectsNavigator {
    
    func navigateToTracker(sprint: Sprint, group: Group) {
        self.scene.push(TrackerScene(group: group, sprint: sprint))
    }
    
    func navigateBackToSprints() {
        self.scene.pop()
    }
}
