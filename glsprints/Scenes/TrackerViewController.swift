//
//  TrackerViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PresentationLayer
import DomainLayer

class TrackerViewController: ViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var sprint = PublishSubject<Sprint>()
    var members = PublishSubject<[Member]>()
    var membersAmount = 0
    
    var presenter: TrackerPresenter<TrackerViewController>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter?.attachView(view: self)
    }

}

extension TrackerViewController: TrackerViewIO {
    
    var backButtonPressed: Signal<Void> {
        return backButton.tapSignal
    }
    
    func showLoadingError(_ error: ErrorWithRecovery) {
        showError(error)
    }
    
    func setSprintInfo(_ sprint: Sprint) {
        self.sprint.onNext(sprint)
    }
    
    func showMembers(_ members: [Member]) {
        self.membersAmount = members.count
        self.members.onNext(members)
    }
}

extension TrackerViewController {
    
    fileprivate func setupSubviews() {
        setupTitleLabel()
        setupTableView()
    }
    
    private func setupTitleLabel() {
        sprint
            .subscribe(
                onNext: { [weak self] sprint in
                    self?.titleLabel.attributedText = "\(sprint.title)\n\(R.string.localizable.team_title())".titleStyled
            })
            .disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "TrackerCell",
                                 bundle: nil), forCellReuseIdentifier: "TrackerCell")
        members
            .bind(to: tableView.rx.items(cellIdentifier: "TrackerCell", cellType: TrackerCell.self)) { [weak self] row, element, cell in
                guard let `self` = self else { return }
                cell.setupWithMember(element, separatorHidden: row == self.membersAmount - 1)
        }.disposed(by: disposeBag)
    }
}
