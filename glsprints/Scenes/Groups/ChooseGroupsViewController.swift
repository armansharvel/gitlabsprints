//
//  GroupsViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 21.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Rswift
import PresentationLayer
import DomainLayer

class ChooseGroupsViewController: ViewController {
    
    @IBOutlet weak var followGroupsButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabelHeight: NSLayoutConstraint!
    
    var groups = PublishSubject<[Group]>()

    var presenter: ChooseGroupsPresenter<ChooseGroupsViewController>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter?.attachView(view: self)
    }
}

// MARK: - ChooseGroupsView
extension ChooseGroupsViewController: ChooseGroupsViewIO {

    //// Show available groups
    func showGroups(_ groups: [Group]) {
        self.groups.onNext(groups)
        tableViewHeight.constant = calcTableViewHeight()
    }

    /// User clicks button to follow chosen groups
    var followGroupsButtonClicked: Signal<Void> {
        return followGroupsButton.tapSignal
    }
    
    var tableViewDidSelectGroup: Driver<Int> {
        return tableView.rx.itemSelected
            .map { [unowned self] indexPath -> Group in try self.tableView.rx.model(at: indexPath) }
            .map { $0.uid }
            .asDriver(onErrorDriveWith: .never())
    }
    
    var tableViewDidDeselectGroup: Driver<Int> {
        return tableView.rx.itemDeselected
            .map { [unowned self] indexPath -> Group in try self.tableView.rx.model(at: indexPath) }
            .map { $0.uid }
            .asDriver(onErrorDriveWith: .never())
    }
    
    /// Loading error occured
    func showGroupsLoadingError(_ error: ErrorWithRecovery) {
        showError(error)
    }
}

// MARK: - UI Setup
extension ChooseGroupsViewController {
    
    /// Sets up subviews of the view
    fileprivate func setupSubviews() {
        setupLabelAndButton()
        setupTableView()
    }
    
    /// Sets up a label and a button of the view
    private func setupLabelAndButton() {
        followGroupsButton.setTitle(R.string.localizable.groups_button(), for: .normal)
        titleLabel.attributedText = R.string.localizable.groups_title().localized.titleStyled + R.string.localizable.groups_subtitle().subtitleStyled
        titleLabelHeight.constant = titleLabel.heightThatFits
    }
    
    /// Sets up the table view
    private func setupTableView() {
        tableView.register(UINib(nibName: "GroupCell", bundle: nil), forCellReuseIdentifier: "GroupCell")
        tableViewDataSourceSetup()
        tableViewDelegateSetup()
    }
    
    private func tableViewDataSourceSetup() {
        
        groups
            .bind(to: tableView.rx.items(cellIdentifier: "GroupCell", cellType: GroupCell.self)) { (row, element, cell) in
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.setupCellWithGroup(element)
            }
            .disposed(by: self.disposeBag)
    }
    
    private func tableViewDelegateSetup() {
        
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                let cell = self?.tableView.cellForRow(at: indexPath) as? GroupCell
                cell?.updateFollowingState()
            }).addDisposableTo(disposeBag)
        
        tableView.rx.itemDeselected
            .subscribe(onNext: { [weak self] indexPath in
                let cell = self?.tableView.cellForRow(at: indexPath) as? GroupCell
                cell?.updateFollowingState()
            }).addDisposableTo(disposeBag)
    }
}

// MARK: - Helper Methods
extension ChooseGroupsViewController {
    
    /// Calculates an actual table view height for laying out on the screen
    ///
    /// - Returns: An actual table view height for laying out on the screen
    fileprivate func calcTableViewHeight() -> CGFloat {
        
        let tableViewNeededHeight = CGFloat(tableView.numberOfRows(inSection: 0)) * tableView.rowHeight
        let tableViewMaxHeight = calcTableViewMaxHeight()
        tableView.isScrollEnabled = tableViewNeededHeight > tableViewMaxHeight
        
        return tableViewNeededHeight > tableViewMaxHeight ? tableViewMaxHeight : tableViewNeededHeight
    }
    
    /// Calculates maximum available room for table view's height
    ///
    /// - Returns: Maximum available room for table view's height
    private func calcTableViewMaxHeight() -> CGFloat {
        let followGroupsButtonY = followGroupsButton.frame.origin.y
        let titleLabelBottom = titleLabel.frame.origin.y + titleLabel.frame.size.height
        let interTitleTableViewMargin: CGFloat = 8
        
        return followGroupsButtonY - titleLabelBottom - interTitleTableViewMargin
    }
}


