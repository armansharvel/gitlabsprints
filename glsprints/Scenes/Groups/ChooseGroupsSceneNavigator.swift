//
//  GroupsSceneNavigator.swift
//  glsprints
//
//  Created by Andrew Ousenko on 27/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer

class ChooseGroupsSceneNavigator : BaseNavigator, ChooseGroupsNavigator {
    public func navigateToDashboard() {
        scene.setFirst(DashboardScene())
    }
}
