//
//  GroupsSceneAssembly.swift
//  glsprints
//
//  Created by Arman Arutyunov on 25.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

/// Groups scene / DI container builder
class ChooseGroupsScene: Scene {
    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(ChooseGroupsInteractor.self) { r in
                ChooseGroupsInteractor(executors: r.resolve(Executors.self)!,
                                       repo: r.resolve(GroupsRepository.self)!)
            }
            container.register(ChooseGroupsPresenter.self) { r -> ChooseGroupsPresenter<ChooseGroupsViewController> in
                ChooseGroupsPresenter(interactor: r.resolve(ChooseGroupsInteractor.self)!,
                                      navigator: r.resolve(ChooseGroupsNavigator.self)!)
            }
            container.register(ChooseGroupsNavigator.self) { _ in
                ChooseGroupsSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = ChooseGroupsViewController()
                vc.presenter = r.resolve(ChooseGroupsPresenter.self)
                return vc
            }
        }
    }
}

