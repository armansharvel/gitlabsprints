//
//  RootScene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 28.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject

class RootScene: Scene {
    override func buildContainer(parent: Container?) -> Container {
        return Container() { container in
            container.register(UINavigationController.self) { _ in
                let rootController = UINavigationController()
                rootController.isNavigationBarHidden = true
                return rootController
            }.inObjectScope(.container)
        }
    }

    func plugInWindow(_ window: UIWindow) {
        window.rootViewController = container().resolve(UINavigationController.self)
        window.makeKeyAndVisible()
    }
}
