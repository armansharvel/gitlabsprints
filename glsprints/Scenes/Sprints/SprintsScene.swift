//
//  SprintsScene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import PresentationLayer
import DomainLayer

/// Auth scene / DI container builder
class SprintsScene: Scene {
    let group: Group
    init(group: Group) {
        self.group = group
    }

    override func buildContainer(parent: Container?) -> Container {
        return Container(parent: parent) { container in
            container.register(SprintsInteractor.self) { r in
                SprintsInteractor(executors: r.resolve(Executors.self)!,
                                  repo: r.resolve(SprintsRepository.self)!,
                                  group: self.group)
            }
            container.register(SprintsPresenter.self) { r -> SprintsPresenter<SprintsViewController> in
                SprintsPresenter(interactor: r.resolve(SprintsInteractor.self)!,
                                 navigator: r.resolve(SprintsNavigator.self)!)
            }
            container.register(SprintsNavigator.self) {_ in
                SprintsSceneNavigator(scene: self)
            }
            container.register(UIViewController.self) { r in
                let vc = SprintsViewController()
                vc.presenter = r.resolve(SprintsPresenter.self)
                return vc
            }
        }
    }
}
