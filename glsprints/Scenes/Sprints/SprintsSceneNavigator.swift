//
//  SprintsSceneNavigator.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import PresentationLayer
import DomainLayer

class SprintsSceneNavigator: BaseNavigator, SprintsNavigator {

    func navigateToProjects(sprint: Sprint, group: Group) {
        scene.push(ProjectsScene(sprint: sprint, group: group))
    }
    
	func navigateBackToGroups() {
		scene.pop()
	}
}
