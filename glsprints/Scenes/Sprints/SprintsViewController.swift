//
//  SprintsViewController.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PresentationLayer
import DomainLayer

class SprintsViewController: ViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var avatarLetter: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trackerButton: UIButton!
    
    @IBOutlet weak var placeholderLabel: UILabel!
    
    fileprivate var sprints = PublishSubject<[Sprint]>()
    fileprivate var group = PublishSubject<Group>()
    
    var presenter: SprintsPresenter<SprintsViewController>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter?.attachView(view: self)
    }
}

extension SprintsViewController: SprintsViewIO {
    
    var backButtonPressed: Signal<Void> {
        return backButton.tapSignal
    }
    
    var tableViewDidSelectSprint: Driver<Int> {
        return tableView.rx.itemSelected
            .map { [unowned self] indexPath -> Sprint in try self.tableView.rx.model(at: indexPath) }
            .map { $0.uid }
            .asDriver(onErrorDriveWith: .never())
    }
    
    func showGroupInfo(_ group: Group) {
        self.group.onNext(group)
    }
    
    func showSprints(_ sprints: [Sprint]) {
        self.sprints.onNext(sprints)
        tableViewHeight.constant = tableView.rowHeight * CGFloat(sprints.count)
        placeholderLabel.isHidden = sprints.count > 0
    }
    
    func showLoadingError(_ error: ErrorWithRecovery) {
        showError(error)
    }
}

// MARK: - UI Setup
extension SprintsViewController {
    fileprivate func setupSubviews() {
        setupGroupInfoView()
        setupTableView()
    }
    
    private func setupGroupInfoView() {
        placeholderLabel.text = R.string.localizable.empty_sprints()
        placeholderLabel.isHidden = true
        group
            .subscribe(
                onNext: { [weak self] group in
                    guard let `self` = self else { return }
                    self.titleLabel.attributedText = group.name.titleStyled
                    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.height/2
                    self.avatarView.backgroundColor = UIColor.randomFlatColor()
                    self.avatarLetter.text = group.avatarLetter
                    self.descriptionLabel.attributedText = group.description.descriptionStyled
            })
            .disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: "SprintCell", bundle: nil),
                           forCellReuseIdentifier: "SprintCell")
        
        sprints
            .bind(to: tableView.rx.items(cellIdentifier: "SprintCell", cellType: SprintCell.self)) { row, element, cell in
                cell.setupCellWithSprint(element)
            }
            .disposed(by: disposeBag)
    }
}
