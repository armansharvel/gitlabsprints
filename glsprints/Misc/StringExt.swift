//
//  StringExt.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var localized: String { return NSLocalizedString(self, comment: "") }
    
    var attributed: NSMutableAttributedString { return NSMutableAttributedString(string: self) }
}

extension NSMutableAttributedString {
    
    var range: NSRange { return NSMakeRange(0, length) }
    
    func letterSpacing(_ kern: CGFloat) -> NSMutableAttributedString {
        addAttribute(NSKernAttributeName, value: kern, range: range)
        return self
    }
    
    func backgroundColor(_ color: UIColor) -> NSMutableAttributedString {
        addAttribute(NSBackgroundColorAttributeName, value: color, range: range)
        return self
    }
    
    func textColor(_ color: UIColor) -> NSMutableAttributedString {
        addAttribute(NSForegroundColorAttributeName, value: color, range: range)
        return self
    }
    
    func font(_ font: UIFont) -> NSMutableAttributedString {
        addAttribute(NSFontAttributeName, value: font, range: range)
        return self
    }
    
    func lineSpacing(_ lineSpacing: CGFloat, alignment: NSTextAlignment = .left) -> NSMutableAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = alignment
        addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: range)
        return self
    }
    
    func lineHeight(_ multiple: CGFloat) -> NSMutableAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = multiple
        addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: range)
        return self
    }
}

func + (left: NSMutableAttributedString, right: NSMutableAttributedString) -> NSMutableAttributedString
{
    let result = NSMutableAttributedString()
    result.append(left)
    result.append(right)
    return result
}
