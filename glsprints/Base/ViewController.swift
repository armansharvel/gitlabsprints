//
//  ViewController.swift
//  glsprints
//
//  Created by Andrew Ousenko on 14/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import Rswift
import RxSwift
import PresentationLayer

class ViewController: UIViewController {
    
    let disposeBag = DisposeBag()

    /// Show alert with error
    func showError(_ error: ErrorWithRecovery) {
        let alert = UIAlertController(title: R.string.localizable.error(), message: error.errorDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: R.string.localizable.ok(), style: .cancel, handler: nil))

        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }

    /// Indicate loading progress
    private let _activityQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    private let _activityIndicator: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.hidesWhenStopped = true
        v.color = UIColor.randomFlatColor()
        return v
    }()
    func show(loading: Bool) {
        let operation = BlockOperation()
        operation.addExecutionBlock { [weak operation] _ in
            usleep(useconds_t(300_000)) // 300 ms
            guard let op = operation else { return }
            if op.isCancelled { return }

            DispatchQueue.main.async {
                if loading {
                    self.view.addSubview(self._activityIndicator)
                    self._activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                    self._activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20).isActive = true
                    self._activityIndicator.startAnimating()
                }
                else {
                    self._activityIndicator.stopAnimating()
                }
                UIView.animate(withDuration: 0.25) { self.view.alpha = loading ? 0.75 : 1 }
                self.view.isUserInteractionEnabled = !loading
            }
        }
        _activityQueue.cancelAllOperations()
        _activityQueue.addOperation(operation)
    }
}

