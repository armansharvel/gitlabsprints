//
//  Navigator.swift
//  glsprints
//
//  Created by Andrew Ousenko on 27/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject

/// Base Navigator for application. Holds reference to associated DI container.

class BaseNavigator {
    
    let scene: Scene
    
    init(scene: Scene) {
        self.scene = scene
    }
}
