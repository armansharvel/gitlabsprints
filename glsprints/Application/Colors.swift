//
//  Colors.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit

extension UIColor {
    class var buttonBg: UIColor {
        return UIColor(red: 25.0 / 255.0, green: 64.0 / 255.0, blue: 154.0 / 255.0, alpha: 1.0)
    }

    class var buttonTitle: UIColor {
        return UIColor.white
    }    

    class var title: UIColor {
        return UIColor(red: 1.0 / 255.0, green: 28.0 / 255.0, blue: 75.0 / 255.0, alpha: 1.0)
    }

    class var subtitle: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 160.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0)
    }
	
	class var dueDateGreen: UIColor {
		return UIColor(red: 13.0 / 255.0, green: 104 / 255.0, blue: 40 / 255.0, alpha: 1.0)
	}
	
	class var expiredDueDateRed: UIColor {
		return UIColor(red: 228.0 / 255.0, green: 60.0 / 255.0, blue: 66.0 / 255.0, alpha: 1.0)
	}
    
    class func randomFlatColor() -> UIColor {
        
        let colors = [
            UIColor(red: 210/255, green: 77/255, blue: 87/255, alpha: 1.0),
            UIColor(red: 217/255, green: 30/255, blue: 24/255, alpha: 1.0),
            UIColor(red: 150/255, green: 40/255, blue: 27/255, alpha: 1.0),
            UIColor(red: 103/255, green: 65/255, blue: 114/255, alpha: 1.0),
            UIColor(red: 68/255, green: 108/255, blue: 179/255, alpha: 1.0),
            UIColor(red: 210/255, green: 77/255, blue: 87/255, alpha: 1.0),
            UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1.0),
            UIColor(red: 51/255, green: 110/255, blue: 123/255, alpha: 1.0),
            UIColor(red: 34/255, green: 49/255, blue: 63/255, alpha: 1.0),
            UIColor(red: 30/255, green: 139/255, blue: 195/255, alpha: 1.0),
            UIColor(red: 58/255, green: 83/255, blue: 155/255, alpha: 1.0),
            UIColor(red: 52/255, green: 73/255, blue: 94/255, alpha: 1.0),
            UIColor(red: 37/255, green: 116/255, blue: 169/255, alpha: 1.0),
            UIColor(red: 31/255, green: 58/255, blue: 147/255, alpha: 1.0),
            UIColor(red: 38/255, green: 166/255, blue: 91/255, alpha: 1.0),
            UIColor(red: 3/255, green: 201/255, blue: 169/255, alpha: 1.0),
            UIColor(red: 27/255, green: 188/255, blue: 155/255, alpha: 1.0),
            UIColor(red: 27/255, green: 163/255, blue: 156/255, alpha: 1.0),
            UIColor(red: 46/255, green: 204/255, blue: 113/255, alpha: 1.0),
            UIColor(red: 22/255, green: 160/255, blue: 133/255, alpha: 1.0),
            UIColor(red: 1/255, green: 152/255, blue: 117/255, alpha: 1.0),
            UIColor(red: 3/255, green: 166/255, blue: 120/255, alpha: 1.0),
            UIColor(red: 77/255, green: 175/255, blue: 124/255, alpha: 1.0),
            UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0),
            UIColor(red: 30/255, green: 130/255, blue: 76/255, alpha: 1.0),
            UIColor(red: 4/255, green: 147/255, blue: 114/255, alpha: 1.0),
            UIColor(red: 38/255, green: 194/255, blue: 129/255, alpha: 1.0),
            UIColor(red: 232/255, green: 126/255, blue: 4/255, alpha: 1.0),
            UIColor(red: 211/255, green: 84/255, blue: 0/255, alpha: 1.0),
            UIColor(red: 249/255, green: 105/255, blue: 14/255, alpha: 1.0),
            UIColor(red: 108/255, green: 122/255, blue: 137/255, alpha: 1.0)
            ]
        
        let randomIdx = Int(arc4random_uniform(UInt32(colors.count)))
        return colors[randomIdx]
    }
}


