//
//  ViewHeight.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/21/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit

extension UIView {
    var heightThatFits: CGFloat {
        return self.sizeThatFits(CGSize(width: self.frame.size.width,
                                        height: CGFloat.greatestFiniteMagnitude)).height
    }
    
    var widthThatFits: CGFloat {
        return self.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude,
                                        height: self.frame.size.height)).width
    }
}
