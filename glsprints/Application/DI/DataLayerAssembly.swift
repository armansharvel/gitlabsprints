//
//  Dependencies.swift
//  glsprints
//
//  Created by Andrew Ousenko on 26/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject
import DomainLayer
import DataLayer

/**
 DataLayer assembler. Builds services and use cases.
 */
class DataLayerAssembly: Assembly {
    func assemble(container: Container) {
        container.register(Executors.self) { _ in StandardExecutors() }
        container.register(AuthRepository.self) { _ in AuthRepositoryImpl() }
        container.register(GroupsRepository.self) { _ in GroupsRepoImpl() }
        container.register(SprintsRepository.self) { _ in SprintsRepoImpl() }
        container.register(ProjectsRepository.self) { _ in ProjectsRepoImpl() }
        container.register(TrackerRepository.self) { _ in TrackerRepoImpl() }
    }
}
