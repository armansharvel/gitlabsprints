//
//  Application.swift
//
//  Created by Arman Arutyunov on 10.07.17.
//  Copyright © 2017 vice3.agency. All rights reserved.
//

import UIKit
import Swinject

/// Main application class. Manages app entry point and holds its root container.
class Application {
    private static let shared = Application()

    private let rootScene: RootScene = {
        // Setup root scene with root container
        let root = RootScene()

        // Inject DataLayer dependencies in root container
        let assembler = Assembler(container: root.container())
        assembler.apply(assembly: DataLayerAssembly())

        return root
    }()

    /// App's entry point
    class func configure(_ window: UIWindow) -> UIWindow {
        shared.rootScene.plugInWindow(window)

        // Add first scene
        let scene = WelcomeScene()
        shared.rootScene.push(scene)

        return window
    }
}
