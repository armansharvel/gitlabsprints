//
//  Scene.swift
//  glsprints
//
//  Created by Arman Arutyunov on 28.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Swinject

/// Scene class. Provides one view controller with all dependencies. Supports parent-child hierarchy.
class Scene {
    private weak var parent: Scene?
    private var _container: Container?

    /// Lazy accessor to internal DI container which is built after `self` and `parent` will be initialized
    final func container() -> Container {
        if _container == nil {
            _container = buildContainer(parent: parent?.container())
        }
        return _container!
    }

    /// Container builder. Should be overriden in subclasses
    open func buildContainer(parent: Container?) -> Container {
        fatalError("not implemented")
    }

    /// Set new scene as first scene in navigation stack
    func setFirst(_ scene: Scene, animated: Bool = true) {
        scene.parent = root
        container().resolve(UINavigationController.self)?.setViewControllers([scene.viewController], animated: animated)
    }

    /// Present child scene modally
    func present(_ scene: Scene, animated: Bool = true, completion: (()->())? = nil) {
        scene.parent = self
        container().resolve(UINavigationController.self)?.present(scene.viewController, animated: animated, completion: completion)
    }

    /// Push child scene in navigation stack
    func push(_ scene: Scene, animated: Bool = true) {
        scene.parent = self
        container().resolve(UINavigationController.self)?.pushViewController(scene.viewController, animated: animated)
    }

    /// Pop child scene from navigation stack
	func pop(animated: Bool = true) {
		container().resolve(UINavigationController.self)?.popViewController(animated: animated)
	}

    /// Associated scene's view controller 
    var viewController: UIViewController {
        return container().resolve(UIViewController.self)!
    }

    /// Root scene
    private var root: Scene? {
        var node = parent
        while node?.parent != nil {
            node = node?.parent
        }
        return node
    }
}
