//
//  Fonts.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import Rswift

extension UIFont {
    class var titleFont: UIFont { return R.font.inputMonoCondensedRegular(size: 32)! }

    class var subtitleFont: UIFont { return R.font.inputMonoCondensedRegular(size: 12)! }

    class var buttonFont: UIFont { return R.font.inputMonoCondensedRegular(size: 18)! }
}
