//
//  AppDelegate.swift
//  glsprints
//
//  Created by Andrew Ousenko on 14/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import Reqres

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setup3rdParty()
        window = Application.configure(UIWindow(frame: UIScreen.main.bounds))
        return true
    }

    func setup3rdParty() {
        Reqres.register()
    }

}

