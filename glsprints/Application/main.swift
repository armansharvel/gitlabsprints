//
//  main.swift
//  glsprints
//
//  Created by Arman Arutyunov on 02.11.2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit

let appDelegateClass: AnyClass? = NSClassFromString("XCTestCase") != nil ? TestingAppDelegate.self : AppDelegate.self
let args = UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory(to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc))

UIApplicationMain(CommandLine.argc, args, nil, NSStringFromClass(appDelegateClass!))
