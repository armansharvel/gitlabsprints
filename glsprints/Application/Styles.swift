//
//  Decorators.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var titleStyled: NSMutableAttributedString {
        return self.attributed.letterSpacing(0.4).textColor(UIColor.title).font(UIFont.titleFont)
    }

    var subtitleStyled: NSMutableAttributedString {
        return self.attributed.letterSpacing(0.4).textColor(UIColor.subtitle).font(UIFont.subtitleFont)
    }
    
    var descriptionStyled: NSMutableAttributedString {
        return self.attributed.letterSpacing(0.4).textColor(UIColor.darkGray).font(UIFont.subtitleFont)
    }
}
