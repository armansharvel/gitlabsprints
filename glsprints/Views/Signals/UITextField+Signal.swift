//
//  UITextField+Signal.swift
//  glsprints
//
//  Created by Arman Arutyunov on 30.08.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import PresentationLayer

extension UITextField {
    var textSignal: Signal<String> {
        let signal: Signal<String> = getAssociatedSignal()
        if !allTargets.contains(self) {
            addTarget(self, action: #selector(emitTextSignal), for: [.editingChanged, .valueChanged])
        }
        return signal
    }

    @objc private func emitTextSignal() {
        let signal: Signal<String> = getAssociatedSignal()
        signal.send(text ?? "")
    }
}
