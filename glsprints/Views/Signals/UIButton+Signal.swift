//
//  UIButton+Signal.swift
//  glsprints
//
//  Created by Arman Arutyunov on 30.08.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import PresentationLayer

extension UIButton {
    var tapSignal: Signal<Void> {
        let signal: Signal<Void> = getAssociatedSignal()
        if !allTargets.contains(self) {
            addTarget(self, action: #selector(emitTapSignal), for: [.primaryActionTriggered])
        }
        return signal
    }

    @objc private func emitTapSignal() {
        let signal: Signal<Void> = getAssociatedSignal()
        signal.send()
    }
}
