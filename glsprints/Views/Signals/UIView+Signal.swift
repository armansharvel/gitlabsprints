//
//  UIView+Signal.swift
//  glsprints
//
//  Created by Arman Arutyunov on 30.08.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import ObjectiveC
import PresentationLayer

private var _handle: UInt8 = 0

extension UIView {
    func getAssociatedSignal<T>() -> Signal<T> {
        if let sig = objc_getAssociatedObject(self, &_handle) as? Signal<T> {
            return sig
        }
        else {
            let sig = Signal<T>()
            objc_setAssociatedObject(self, &_handle, sig, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return sig
        }
    }
}
