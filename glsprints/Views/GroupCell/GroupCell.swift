//
//  GroupCell.swift
//  glsprints
//
//  Created by Arman Arutyunov on 26/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import DomainLayer

class GroupCell: UITableViewCell {
	
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var groupDescription: UILabel!
	@IBOutlet weak var checkmark: UIImageView!
	@IBOutlet weak var avatarView: UIView!
	@IBOutlet weak var avatarLetter: UILabel!
	
	@IBOutlet weak var descriptionHeight: NSLayoutConstraint!
	
	override func awakeFromNib() {
		super.awakeFromNib()

        selectionStyle = .none

		avatarView.layer.cornerRadius = avatarView.frame.size.height/2
		avatarView.backgroundColor = UIColor.randomFlatColor()
	}
		
	func setupCellWithGroup(_ group: Group) {
		name.text = group.name
		groupDescription.attributedText = group.description.descriptionStyled
        setDescriptionHeight()
		updateFollowingState()
		avatarLetter.text = group.avatarLetter
	}
    
    func setupCellWithGoal(_ goal: Issue) {
        name.text = goal.title
        groupDescription.attributedText = goal.description.descriptionStyled
        setDescriptionHeight()
        checkmark.image = goal.state == .closed ? UIImage(named: "checked") : UIImage()
        avatarLetter.text = goal.avatarLetter
    }
    
    private func setDescriptionHeight() {
        descriptionHeight.constant = groupDescription.text == "" ? 0 : 20
    }
	
	func updateFollowingState() {
		checkmark.image = self.isSelected ? UIImage(named: "checked") : UIImage()
	}
}
