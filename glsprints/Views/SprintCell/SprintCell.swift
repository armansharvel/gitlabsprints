//
//  SprintCell.swift
//  glsprints
//
//  Created by Arman Arutyunov on 26/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import DomainLayer

class SprintCell: UITableViewCell {
	@IBOutlet weak var nameAndCompletionLabel: UILabel!

	@IBOutlet weak var progressView: UIView!
	@IBOutlet weak var progressBar: UIView!
	@IBOutlet weak var progressBarWidth: NSLayoutConstraint!
	
	@IBOutlet weak var startDateLabel: UILabel!
	@IBOutlet weak var dueDateLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

		progressView.backgroundColor = UIColor.white
		progressView.layer.borderColor = UIColor.lightGray.cgColor
		progressView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setupCellWithSprint(_ sprint: Sprint) {
		nameAndCompletionLabel.text = "\(sprint.title) : \(Int(round(sprint.completion)))%"
		progressBarWidth.constant = CGFloat(self.contentView.frame.size.width - 16 - 16) * CGFloat(sprint.completion / 100)
		startDateLabel.text = sprint.startDate.toString()
		if let date = sprint.dueDate {
            dueDateLabel.isHidden = false
			dueDateLabel.text = date.toString()
			dueDateLabel.textColor = date > Date() ? UIColor.dueDateGreen : UIColor.expiredDueDateRed
		} else {
            dueDateLabel.isHidden = true
			dueDateLabel.text = ""
		}
	}
    
    func setupCellWithProject(_ project: Project) {
        nameAndCompletionLabel.text = project.name
        progressBarWidth.constant = CGFloat(self.contentView.frame.size.width - 16 - 16) * CGFloat(project.completion / 100)
        startDateLabel.text = "\(project.getIssues(open: false).count) / \(project.issues.count) complete"
        dueDateLabel.isHidden = true
    }
	
	
}





