//
//  ActionButton.swift
//  glsprints
//
//  Created by Arman Arutyunov on 24.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit

@IBDesignable class ActionButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func prepareForInterfaceBuilder() {
        setup()
    }

    private func setup()
    {
        setTitleColor(UIColor.buttonTitle, for: .normal)
        backgroundColor = UIColor.buttonBg
        titleLabel?.font = UIFont.buttonFont
    }

}
