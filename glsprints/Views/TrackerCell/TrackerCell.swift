//
//  TrackerCell.swift
//  glsprints
//
//  Created by Arman Arutyunov on 10/2/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import UIKit
import DomainLayer

class TrackerCell: UITableViewCell {

    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var avatarLetter: UILabel!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var openIssuesAmount: UILabel!
    @IBOutlet weak var openEstimated: UILabel!
    @IBOutlet weak var openSpent: UILabel!
    
    @IBOutlet weak var closedIssuesAmount: UILabel!
    @IBOutlet weak var closedEstimated: UILabel!
    @IBOutlet weak var closedSpent: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarView.layer.cornerRadius = avatarView.frame.size.height/2
        avatarView.backgroundColor = UIColor.randomFlatColor()
    }
    
    func setupWithMember(_ member: Member, separatorHidden: Bool) {
        avatarLetter.text = member.avatarLetter
        username.text = member.username
        
        let timeStats = member.timeStats
        
        openIssuesAmount.text = String(timeStats.openIssuesAmount)
        openEstimated.text = String(timeStats.openEstimatedTime / 3600) + "h"
        openSpent.text = String(timeStats.openSpentTime / 3600) + "h"
        
        closedIssuesAmount.text = String(timeStats.closedIssuesAmount)
        closedEstimated.text = String(timeStats.closedEstimatedTime / 3600) + "h"
        closedSpent.text = String(timeStats.closedSpentTime / 3600) + "h"
        
        separatorView.isHidden = separatorHidden
    }
}
