//
//  TimeStatsResponse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 10/2/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss
import DomainLayer

public struct TimeStatsResponse: Gloss.Decodable {
    let estimated: Int
    let spent: Int
    
    public init?(json: JSON) {
        guard
            let estimated: Int = "time_estimate" <~~ json,
            let spent: Int = "total_time_spent" <~~ json
        else { return nil }
        
        self.estimated = estimated
        self.spent = spent
    }
}

extension TimeStatsResponse {
    func toDomain() -> TimeStats {
        return TimeStats(estimated: estimated, spent: spent)
    }
}
