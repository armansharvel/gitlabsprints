//
//  SprintsResponse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss
import DomainLayer

public struct SprintResponse: Gloss.Decodable {
    let id: Int
    let title: String
    let startDate: Date
    let dueDate: Date?
    
    public init?(json: JSON) {
        guard
            let id: Int = "id" <~~ json,
            let title: String = "title" <~~ json,
            let startDate: String = "start_date" <~~ json
        else { return nil }
        
        let dueDate: String? = "due_date" <~~ json
        
        func dateFromString(_ stringDate: String) -> Date? {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.date(from: stringDate) ?? nil
        }
        
        self.id = id
        self.title = title
        self.startDate = dateFromString(startDate)!
        if let due = dueDate {
            self.dueDate = dateFromString(due)
        } else {
            self.dueDate = nil
        }
    }
}

extension Sprint: Gloss.Encodable {
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> uid,
            "title" ~~> title,
            "start_date" ~~> startDate,
            "due_date" ~~> dueDate,
            "issues" ~~> issues,
            "completion" ~~> completion
            ])
    }
}

extension SprintResponse {
    func toDomain(withIssues issues: [Issue]) -> Sprint {
        return Sprint(uid: id, title: title, startDate: startDate, dueDate: dueDate, issues: issues)
    }
}
