//
//  ProjectsResponse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 28/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss
import DomainLayer

public struct ProjectResponse: Gloss.Decodable {
    let id: Int
    let name: String
    
    public init?(json: JSON) {
        guard
            let id: Int = "id" <~~ json,
            let name: String = "name" <~~ json
        else { return nil }
        
        self.id = id
        self.name = name
    }
}

extension ProjectResponse {
    func toDomain(with issues: [Issue]) -> Project {
        return Project(uid: id, name: name, issues: issues)
    }
}
