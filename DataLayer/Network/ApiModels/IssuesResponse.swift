//
//  IssuesResponse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/25/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss
import DomainLayer

public struct IssueResponse: Gloss.Decodable {
    let id: Int
    let projectId: Int
    let title: String
    let description: String
    let state: String
    let assigneeId: Int
    let assigneeName: String
    let timeStats: TimeStatsResponse
    let isGoal: Bool
    
    public init?(json: JSON) {
        guard
            let id: Int = "id" <~~ json,
            let projectId: Int = "project_id" <~~ json,
            let title: String = "title" <~~ json,
            let state: String = "state" <~~ json,
            let assigneeId: Int = "assignee.id" <~~ json,
            let assigneeName: String = "assignee.name" <~~ json
        else { return nil }
        
		let description: String? = "description" <~~ json
        let labels: [String]? = "labels" <~~ json
        let timeStats: TimeStatsResponse? = "time_stats" <~~ json
        
        self.id = id
        self.projectId = projectId
        self.title = title
        self.description = description ?? ""
        self.state = state
        self.assigneeId = assigneeId
        self.assigneeName = assigneeName
        self.timeStats = timeStats ?? TimeStatsResponse(json: ["time_estimate" : 0, "total_time_estimate" : 0])!
        
        let lowercasedLabels = labels?.map { $0.lowercased() }
        self.isGoal = lowercasedLabels?.contains("goal") ?? false
    }
}

extension IssueResponse {
    func toDomain() -> Issue {
        return Issue(id: id, projectId: projectId, title: title, description: description, state: state, assigneeId: assigneeId, assigneeName: assigneeName, timeStats: timeStats.toDomain(), isGoal: isGoal)
    }
}
