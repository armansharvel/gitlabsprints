//
//  File.swift
//  glsprints
//
//  Created by Andrew Ousenko on 27/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss

struct SignInResponse : Gloss.Decodable {

    let accessToken : String
    let expiresIn: Int
    
    init?(json: JSON) {
        guard
            let token: String = "access_token" <~~ json,
            let expires: Int = "expires_in" <~~ json
        else { return nil }
        
        expiresIn = expires
        accessToken = token
    }
}
