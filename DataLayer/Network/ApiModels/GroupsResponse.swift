//
//  GroupsResponse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Gloss
import DomainLayer

struct GroupResponse: Gloss.Decodable {
    
    let uid: Int
    let name: String
    let description: String
    
    init?(json: JSON) {
        guard
            let uid: Int = "id" <~~ json,
            let name: String = "name" <~~ json,
            let description: String = "description" <~~ json
            else { return nil }
        
        self.uid = uid
        self.name = name
        self.description = description
    }
}

extension Group: Gloss.Encodable {
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> uid,
            "name" ~~> name,
            "description" ~~> description
            ])
    }
}

extension GroupResponse {
    func toDomain() -> Group {
        return Group(uid: uid, name: name, description: description)
    }
}
