//
//  Response+Parse.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer
import Moya
import Gloss

extension Response {
    func parse<T>() throws -> T where T: Gloss.Decodable {
        guard
            let jsonData = try? mapJSON() as? [String: Any],
            let json = jsonData
            else { throw NetworkError.badResponse }
        
        guard let o = T(json: json) else { throw NetworkError.badResponse }
        return o
    }
    
    func parseArray<T>() throws -> [T] where T: Gloss.Decodable {
        guard
            let jsonData = try? mapJSON() as? [[String: Any]],
            let json = jsonData
            else { throw NetworkError.badResponse }
        
        var result = [T]()
        for dict in json {
            guard let o = T(json: dict) else { throw NetworkError.badResponse }
            result.append(o)
        }
        
        return result
    }
}
