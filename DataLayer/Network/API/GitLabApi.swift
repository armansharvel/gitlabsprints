//
//  GitLabApi.swift
//  glsprints
//
//  Created by Andrew Ousenko on 27/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar
import DomainLayer

public enum GitLabApi {
    case getGroups
    case getSprints(groupId: Int)
    case getSprintIssues(groupId: Int, sprintId: Int)
    case getProject(id: Int)
}

extension GitLabApi: SugarTargetType, AccessTokenAuthorizable {
    public var baseURL:  URL {
        return URL(string: "https://gitlab.com/api/v4")!
    }

    public var route: Route {
        switch self {
        case .getGroups:
            return .get("/groups")
        case .getSprints(let groupId):
            return .get("/groups/\(groupId)/milestones")
        case .getSprintIssues(let groupId, let sprintId):
            return .get("/groups/\(groupId)/milestones/\(sprintId)/issues")
        case .getProject(let id):
            return .get("/projects/\(id)")
        }
    }

    public var url: URL {
        return defaultURL
    }

    public var params: Parameters? {
        return nil
    }

    public var httpHeaderFields: [String: String]? {
        return nil
    }

    public var sampleData: Data {
        return Data() //stub
    }

    public var task : Task {
        return .request
    }
    
    public var shouldAuthorize: Bool {
        return true
    }
    
}
