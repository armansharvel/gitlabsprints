//
//  GroupsRepoImpl.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift
import Moya
import MoyaSugar

public class GroupsRepoImpl: GroupsRepository {
    private let api: RxMoyaSugarProvider<GitLabApi>
    
    public init(api: RxMoyaSugarProvider<GitLabApi> = MoyaProviderFactory.getInstance()) {
        self.api = api
    }
    
    public func listGroups() -> Observable<[Group]> {
        return api.request(.getGroups)
            .map { response in
                let items: [GroupResponse] = try response.parseArray()
                return items.map { $0.toDomain() }
        }
    }
    
    public func updateFollowingGroups(groups: [Group]) {
        let items = groups.map({ $0.toJSON() ?? [:] })
        UserDefaults.standard.setValue(items, forKey: GroupKeys.followingGroupsKey)
    }
    
    
    public func loadFollowingGroups() -> Observable<[Group]> {
        let items = (UserDefaults.standard.object(forKey: GroupKeys.followingGroupsKey) as? [[String : Any]]) ?? []
        return Observable.just(items.map { GroupResponse(json: $0)?.toDomain() }.flatMap { $0 })
    }    
}
