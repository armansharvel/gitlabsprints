//
//  MoyaProviderFactory.swift
//  DataLayer
//
//  Created by Arman Arutyunov on 14.09.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

class MoyaProviderFactory {
    class func getInstance(tokenProvider: TokenProvider = KeychainTokenProvider()) -> RxMoyaSugarProvider<GitLabApi> {
        if let token = tokenProvider.getToken() {
            let authPlugin = AccessTokenPlugin(token: token)
            return RxMoyaSugarProvider<GitLabApi>(plugins: [authPlugin])
        }
        else {
            return RxMoyaSugarProvider<GitLabApi>()
        }
    }
}
