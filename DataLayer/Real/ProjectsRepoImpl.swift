//
//  ProjectsRepoImpl.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/27/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift
import Moya
import MoyaSugar

public class ProjectsRepoImpl: ProjectsRepository {
    
    private let api: RxMoyaSugarProvider<GitLabApi>
    
    public init(api: RxMoyaSugarProvider<GitLabApi> = MoyaProviderFactory.getInstance()) {
        self.api = api
    }
    
    public func listProjects(of sprint: Sprint) -> Observable<[Project]> {
        let projectsIds = Array(Set(sprint.issues.map { $0.projectId }))
        let projectsObservable = Observable.from(projectsIds).flatMap { [unowned self] id -> Observable<Project> in
            return self.api.request(.getProject(id: id))
                .map { response -> Project in
                    let projectResponse: ProjectResponse = try response.parse()
                    let issues = sprint.issues.filter({ $0.projectId == projectResponse.id })
                    return projectResponse.toDomain(with: issues)
                }
        }
        return projectsObservable.toArray()
    }
    
    public func listGoals(of sprint: Sprint) -> Observable<[Issue]> {
        return Observable.just(sprint.issues.filter{ $0.isGoal })
    }
}
