//
//  SprintsRepoImpl.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift
import Moya
import MoyaSugar

public class SprintsRepoImpl: SprintsRepository {
    
    private let api: RxMoyaSugarProvider<GitLabApi>
    
    public init(api: RxMoyaSugarProvider<GitLabApi> = MoyaProviderFactory.getInstance()) {
        self.api = api
    }
    
    public func listSprints(ofGroup group: Group) -> Observable<[Sprint]> {
        return api.request(.getSprints(groupId: group.uid))
            .map { response -> [SprintResponse] in
                let sprintResponses: [SprintResponse] = try response.parseArray()
                return sprintResponses
            }
            .flatMap { sprints -> Observable<[Sprint]> in
                let sprintResponsesObservable = Observable.from(sprints)
                let sprintsObservable = sprintResponsesObservable.flatMap { [unowned self] sprintResponse -> Observable<Sprint> in
                    return self.getIssues(ofSprint: sprintResponse, ofGroup: group).map { sprintResponse.toDomain(withIssues: $0) }
                }
                return sprintsObservable.toArray()
            }
            .map {
                $0.sorted(by: { $0.startDate < $1.startDate })
            }
    }
    
	private func getIssues(ofSprint sprint: SprintResponse, ofGroup group: Group) -> Observable<[Issue]> {
		return api.request(.getSprintIssues(groupId: group.uid, sprintId: sprint.id))
            .map { response in
                let issues: [IssueResponse] = try response.parseArray()
                return issues.map { $0.toDomain() }
        }
    }

}
