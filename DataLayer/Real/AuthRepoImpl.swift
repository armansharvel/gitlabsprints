//
//  AuthRepoImpl.swift
//  glsprints
//
//  Created by Andrew Ousenko on 27/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift
import Moya
import MoyaSugar
import OAuthSwift

public class AuthRepositoryImpl: AuthRepository {
    
    private let api: RxMoyaSugarProvider<GitLabApi>
    private let oauthKeys = OAuthSetupKeys()
    
    public init(api: RxMoyaSugarProvider<GitLabApi> = MoyaProviderFactory.getInstance()) {
        self.api = api
    }
    
    public func signIn(email: String, password: String) -> Observable<String> {
        return authorize(email: email, password: password)
    }
    
    public func isSignedIn() -> Observable<Bool> {
        return Observable.just(api.plugins.first(where: { $0 is AccessTokenPlugin }) != nil)
    }

    // MARK: Private

    private func getOAuth() -> OAuth2Swift {
        return OAuth2Swift(consumerKey: oauthKeys.consumerKey,
                           consumerSecret: oauthKeys.consumerSecret,
                           authorizeUrl: oauthKeys.authorizeUrl,
                           responseType: oauthKeys.responseType)
    }
    
    private func authorize(email: String, password: String) -> Observable<String> {
        return getOAuth().rx_startAuthorizedRequest(
            "https://gitlab.com/oauth/token",
            method: .POST,
            parameters: ["username": email,
                         "password": password,
                         "grant_type": "password"]
            )
            .map { response in
                guard let json = try? response.jsonObject() as? [String: Any] else { throw NetworkError.badResponse }
                guard let account = GitLabAccount.loadFromJSON(json ?? [:]) else { throw NetworkError.badResponse }
                try account.saveToKeychain()
                return account.accessToken
        }
    }
}
