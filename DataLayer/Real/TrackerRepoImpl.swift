//
//  TrackerRepoImpl.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift
import Moya
import MoyaSugar

public class TrackerRepoImpl: TrackerRepository {
    
    private let api: RxMoyaSugarProvider<GitLabApi>
    
    public init(api: RxMoyaSugarProvider<GitLabApi> = MoyaProviderFactory.getInstance()) {
        self.api = api
    }
    
    public func getMembers(of sprint: Sprint, of group: Group) -> Observable<[Member]> {
        return api.request(.getSprintIssues(groupId: group.uid, sprintId: sprint.uid))
            .map { response -> [Member] in
                let issueResponses: [IssueResponse] = try response.parseArray()
                let issues = issueResponses.map { $0.toDomain() }
                let memberIds = Array(Set(issues.map{ $0.assigneeId }))
                var members = [Member]()
                for id in memberIds {
                    let thisIssues = issues.filter { $0.assigneeId == id }
                    let timeStats = MemberTimeStats()
                    var member = Member(id: id, username: thisIssues[0].assigneeName, timeStats: timeStats)
                    for issue in issues {
                        if issue.assigneeId == id {
                            if issue.state == .closed {
                                member.timeStats.closedIssuesAmount += 1
                                member.timeStats.closedEstimatedTime += issue.timeStats.estimated
                                member.timeStats.closedSpentTime += issue.timeStats.spent
                            } else {
                                member.timeStats.openIssuesAmount += 1
                                member.timeStats.openEstimatedTime += issue.timeStats.estimated
                                member.timeStats.openSpentTime += issue.timeStats.spent
                            }
                        }
                    }
                    members.append(member)
                }
                return members
        }
    }
}
