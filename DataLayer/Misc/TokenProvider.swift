//
//  TokenProvider.swift
//  DataLayer
//
//  Created by Arman Arutyunov on 14.09.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

protocol TokenProvider {
    func getToken() -> String?
}
