//
//  GitLabAccountExt.swift
//  DataLayer
//
//  Created by Arman Arutyunov on 14.09.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer
import Locksmith

extension GitLabAccount {

    private struct Keys {
        static let accessToken = "access_token"
        static let tokenType = "token_type"
        static let grantDate = "grant_date"

        static let service = "GitLab"
    }

    public func saveToKeychain() throws {
        try Locksmith.updateData(data: [Keys.accessToken: accessToken,
                                        Keys.tokenType: tokenType,
                                        Keys.grantDate: grantDate.timeIntervalSince1970],
                                 forUserAccount: Keys.service)
    }

    public static func loadFromKeychain() -> GitLabAccount? {
        guard
            let dictionary = Locksmith.loadDataForUserAccount(userAccount: Keys.service),
            let token = dictionary[Keys.accessToken] as? String,
            let type = dictionary[Keys.tokenType] as? String,
            let grantDateValue = dictionary[Keys.grantDate] as? TimeInterval
            else { return nil }

        return GitLabAccount(accessToken: token,
                             tokenType: type,
                             grantDate: Date(timeIntervalSince1970: grantDateValue))
    }

    public static func loadFromJSON(_ json: [String: Any]) -> GitLabAccount? {
        guard
            let token = json["access_token"] as? String,
            let type = json["token_type"] as? String
            else { return nil }

        return GitLabAccount(accessToken: token,
                             tokenType: type,
                             grantDate: Date())
    }
}
