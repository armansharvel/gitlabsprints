//
//  OAuthRxSwift.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/13/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import OAuthSwift
import RxSwift

extension OAuth2Swift {
    
    @nonobjc open func rx_startAuthorizedRequest(_ url: String, method: OAuthSwiftHTTPRequest.Method, parameters: OAuthSwift.Parameters) -> Observable<OAuthSwiftResponse> {
        
        return Observable<OAuthSwiftResponse>.create{ (observer: AnyObserver<OAuthSwiftResponse>) -> Disposable in
            let handle = self.startAuthorizedRequest(
                url,
                method: method,
                parameters: parameters,
                success: { response in
                    observer.onNext(response)
                    observer.onCompleted()
            },
                failure: { error in
                    observer.onError(error)
            })
            return Disposables.create {
                handle?.cancel()
            }
        }.share()
    }
    
}
