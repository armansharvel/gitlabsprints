//
//  KeychainTokenProvider.swift
//  DataLayer
//
//  Created by Arman Arutyunov on 14.09.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import DomainLayer

class KeychainTokenProvider: TokenProvider {
    func getToken() -> String? {
        return GitLabAccount.loadFromKeychain()?.accessToken
    }
}
