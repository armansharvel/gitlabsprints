//
//  GroupKeys.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/22/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

public struct GroupKeys {
    static let followingGroupsKey = "following_groups"
}
