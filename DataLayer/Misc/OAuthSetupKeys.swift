//
//  OAuthSetupKeys.swift
//  glsprints
//
//  Created by Arman Arutyunov on 9/12/17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation

public struct OAuthSetupKeys {
    public let consumerKey = "2b5b22d58c26fefc2d027a1d6fa252b5a26b8596fab97d0028e681852be56bb0"
    public let consumerSecret = "aa596a83e7d8456d4fd655f9941f4316eed88767b167a930cac82a7abb517d6e"
    public let authorizeUrl = "https://gitlab.com/oauth/token"
    public let responseType = "token"
    
    public init() {}
}
