//
//  StubLogInUseCase.swift
//  glsprints
//
//  Created by Arman Arutyunov on 25.07.17.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import RxSwift
import DomainLayer

public class StubAuthRepository: AuthRepository {
    
    public init(){}
    
    
    /// Sign into Git Lab
    ///
    /// - Parameter token: gitlab token
    /// - Returns: emits user profile corresponding to that token
    public  func signIn(email: String, password: String) -> Observable<String> {
        return Observable.just("token")
    }
    
    
    /// Whether user is signed into GitLab
    ///
    /// - Returns: emits true if user is signed into GitLab
    public func isSignedIn() -> Observable<Bool>{
        return Observable.just(true)
    }
    
    
    /// Get logged in user profile, if user is logged in already
    ///
    /// - Returns: emits logged in user profile or error otherwise
    public func getLoggedInUserProfile() -> Observable<Profile>{
        return getFakeProfile()
    }
    
    private func getFakeProfile () -> Observable<Profile> {
        return Observable.just(Profile("Drew"))
    }
}
