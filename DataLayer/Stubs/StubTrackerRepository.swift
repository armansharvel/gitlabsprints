//
//  StubTrackerRepository.swift
//  glsprints
//
//  Created by Arman Arutyunov on 29/09/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift

public class StubTrackerRepo: TrackerRepository {
    public func getMembers(of sprint: Sprint, of group: Group) -> Observable<[Member]> {
        return Observable.just([Member(id: 1, username: "armansharvel", timeStats: MemberTimeStats())])
    }
}
