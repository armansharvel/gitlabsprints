//
//  StubGroupRepository.swift
//  glsprints
//
//  Created by Andrew Ousenko on 26/07/2017.
//  Copyright © 2017 Andrew Ousenko. All rights reserved.
//

import Foundation
import DomainLayer
import RxSwift

public class StubGroupRepo : GroupsRepository {

    public init() {}
    
    public func listGroups() -> Observable<[Group]> {
        return Observable.just([
            Group(uid: 1, name: "Coca-Cola", description: "Coca Cola outsource"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project")
        ])
    }
    
    public func updateFollowingGroups(groups: [Group]) {
    }

    public func loadFollowingGroups() -> Observable<[Group]> {
        return Observable.just([
            Group(uid: 1, name: "Coca-Cola", description: "Coca Cola outsource"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project"),
            Group(uid: 2, name: "Some Other", description: "Some other project")
            ])
    }
    
    /// Updates current group to show in the Sprints Scene
    public func updateCurrentGroup(group: Group) {
        
    }
    
    public func loadCurrentGroup() -> Observable<Group> {
        return Observable.just(Group(uid: 1, name: "Pepsi Group", description: "Pepsi Group Description"))
    }
}
